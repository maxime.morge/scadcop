// Copyright (C) Maxime MORGE, Alex VIGNERON 2020
package org.scadcop.solver.decentralized.mgm2

import org.scadcop.problem._

/**
  * Class representing an offer
  * @param offererVariable is the variable of the offerer
  * @param moves is a list of coordinated moves
  */
class Offer(val offererVariable : Variable, val moves : List[Move]){
  override def toString: String = s"offerer=$offererVariable moves= ${moves.mkString("[",",","]")}"
}
