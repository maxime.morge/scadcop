\section{Distributed Constraint Optimization Problems}
\label{sec:pb}

A Distributed Constraint Optimization Problem (DCOP) framework is
the distributed version of constraint optimization problems. In this
multi-agent paradigm, agents representing variables in the problem communicate so that each of them
can gradually update the value of the variable it controls to improve the global utility function.

There are at least as many variables as agents and possibly more variables than agents, implying that a single agent might control several variables.
However in most DCOPs, control of a single variable by a single agent is assumed.
The optimal solution is the minimum/maximum of the global objective function.
Each variable has a domain of values it can take, this domain is only known to the agent in control of the said variable. 
However the value of a variable is known to an agent's neighbour.
The notion of neighbourhood is key because DCOPs rely on locality.
Therefore each agent can only communicate with its neighbours and only knows about cost functions which involve at least one of the variables it controls.

\subsection{Origins}
In order to better understand DCOPs, we give here a brief overview of the types of problems they come from.

A CSP is a problem where the goal is to determine whether a set of variables spanning over determined domains can satisfy constraints.
These are typically combinatorial problems \cite{fioretto18jair}

\begin{definition}[CSP]
  \label{def:CSP}
  A Constraint Satisfaction Problem is a tuple 
  $\langle X,D,C \rangle$ where 
  \begin{itemize}
    \item $X = \{x_1, \ldots, x_n \}$ is the the set of variables
    \item $D = \{d_1, \ldots, d_n \}$ is the set of corresponding non-empty domains
    \item $C = \{c_1, \ldots, c_m\}$ is the set of constraints over the variables.
  \end{itemize}
\end{definition}

COPs -sometimes called Weighted Constraint Satisfaction Problems- take a step further when compared with CSPs.
Here the solution is not binary simply stating whether constraint can be satisfied or not but values are quantifiable.
COPs are akin to CSPs with an additional particular objective function, either a maximisation or a minimisation. In this setting, constraints can either be hard or soft depending on whether respecting them is vital or preferable.
There are two types of objective functions, maximisations with their respective gain and minimisations with their respective cost.
Typically, a constraint which \textit{needs} to be satisfied will be coined \textit{hard} while a constraint which \textit{should} be satisfied will be coined \textit{soft}.

\begin{definition}[COP]
  \label{def:COP}
  A Constraint Optimization Problem is a tuple
  $\langle X,D,F,\alpha \rangle$ where:
  \begin{itemize}
  \item $X = \{ x_1, \ldots, x_n \}$ is a set of $n$ variables
  \item $D = \{ D_1, \ldots, D_n \}$ is the set of finite domains for the variables in $X$,
    with $D_i$ being the set of possible values for the variable $x_i$
  \item $F$ is a set of constraints. A constraint $f_i \in F$ is a
    function\\
    $f_i : \Pi_{x_j \in \mathbf{x^i}} D_j \rightarrow \mathds{R^+} \cup \{ \bot \}$,
    where the set of variables
    $\mathbf{x^i} =\{ x_{i_1}, \ldots x_{i_k}\} \subseteq X$ is the scope of
    $f_i$
    and $\bot$ is a special element used to denote that a
    given combination of values for the variables in $\mathbf{x^i}$ is not
    allowed, and it has the property that
    $a + \bot = \bot + a= \bot, \forall~a \in \mathds{R}$.
\end{itemize}
\end{definition}

A distributed version of CSPs also exists in the form of DisCSPs.
DCOPs are decentralised versions of COPs which are in turn extended CSPs.


\begin{definition}[DCOP]
  \label{def:dcop}
  A Distributed Constraint Optimization Problem (DCOP) is a tuple
  $\langle A,X,D,F,\alpha \rangle$ where:
  \begin{itemize}
  \item $A = \{a_1 , \ldots, a_m \}$ is a set of $m$ autonomous agents
  \item $X = \{ x_1, \ldots, x_n \}$ is a set of $n$ variables 
  \item $D = \{ D_1, \ldots, D_n \}$ is the set of finite domains for the variables in $X$,
    with $D_i$ being the set of possible values for the variable $x_i$
  \item $F$ is a set of constraints. A constraint $f_i \in F$ is a
    function\\
    $f_i : \Pi_{x_j \in \mathbf{x^i}} D_j \rightarrow \mathds{R^+} \cup \{ \bot \}$,
    where the set of variables
    $\mathbf{x^i} =\{ x_{i_1}, \ldots x_{i_k}\} \subseteq X$ is the scope of
    $f_i$
    %\footnote{The presence of a fixed ordering of variables is assumed.} 
    and $\bot$ is a special element used to denote that a
    given combination of values for the variables in $\mathbf{x^i}$ is not
    allowed, and it has the property that
    $a + \bot = \bot + a= \bot, \forall~a \in \mathds{R}$
  \item $\alpha : X \rightarrow A$ is a surjective function, from
    variables to agents, which assigns the control of each variable
    $x \in X$ to an agent $\alpha(x)$.
\end{itemize}
\end{definition}


A partial assignment is a value assignment for a proper subset of
variables of $X$.  An assignment is complete if it assigns a value to
each variable in $X$. For a given complete assignment $\sigma$, we say
that a constraint (also called cost function) $f_i$ is satisfied by
$\sigma$ if $f_i(\sigma(x_i)) \neq \bot$. A complete assignment is a
solution of a DCOP if it satisfies all its constraints.

In the case of anytime algorithms, each assignment is complete and gradually improves over time, which makes it possible to stop the algorithm at any point.

The optimization objective is represented by the function $\Obj$, which
can be of different nature, either a minimisation or a maximisation. A solution is a complete assignment with
cost different from $\bot$, and an optimal solution is a solution with
minimal cost (resp. maximal utility). In general, this function is a sum of cost (resp. utility) constraints:
$F = \Sigma_i f_i$ ; but some approaches can use other kind of agregation.

Additionally, in the following report, we consider that:
\begin{itemize}
	\item  $n = m$,
	i.e. each agent controls only one variable. This restriction is often considered in the literature;
	\item constraints are binary constraints. More precisely, $F$ contains at most one function $f_{ij}$ per pair ${i,j}$;
	\item there can be as many constraints as needed for each variable.
\end{itemize}.

With these assumptions, a DCOP can be easily represented as a graph where vertices are agents (each agent owns one variable) and
edges are binary constraints. Note that since constraints are binary and can only be over a given pair once, the resulting graph is never a multigraph.

\begin{definition}
  Let $\langle A,X,D,F,\alpha \rangle$ be a DCOP, such that
  
	We define from $F$ the global cost function and the local cost function of an agent depending on its neighborhood~:
 
	\begin{itemize}
		\item the global cost function $\Obj = \Sigma_{f_{ij} \in F} f_{ij}(x_i,x_j)$ ;
		\item the neighborhood $N_i$ of agent $a_i$ is $\{a_j \in A | f_{ij} \in F \}$ ;
		\item the local cost function of agent $a_i$ is $\contrib{i} = \Sigma_{a_j \in N_i} f_{ij}(x_i,x_j)$ ;
		\item given a valuation $\sigma$ of variables in X, the contribution of agent $a_i$ is the result of its local cost function for this valuation, i.e. $\Sigma_{a_j \in N_i} f_{ij}(\sigma(x_i),\sigma(x_j))$.
	\end{itemize}
	
\end{definition}

We consider here cost functions, so the objective is to minimize the global cost function.
Symmetrically, one can use utility functions and the objective becomes to maximize the global utility function.

\subsection{Classification}
In the multi-agent domain, DCOPs are classified according to their set of characteristics, namely:
\begin{itemize}
    \item Action effects : stochastic or deterministic.
    Refers to the results obtained by actions, in a deterministic seting one action is considered to have one specific result in a particular context, and if this action had to be repeated in a similar context, the result ought to be identical.
    Whereas in a stochastic one, the same action performed in the same environment can lead to a different result.
    \item Knowledge : total or incomplete
    Refers to the extent of an agent's awareness of the other agent's variables.
    With incomplete knowledge, locality is often considered as the landmark, while with total knowledge any agent posesses information about any other agent.
    \item Group behaviour : cooperative or competitive
    Depends on whether agents consider their own welfare first or the general well-being of the system.
    \item Environment type : deterministic or stochastic
    Refers to how an environment evolves, whether events follow a certain pattern or not.
    \item Environment's dynamics : static or dynamic
    Depends on whether an environment remains identical from beginning to end of a process or evolves.  
\end{itemize}
Generally speaking, most DCOP algorithms rely on deterministic action effects, a cooperative group-behaviour and total knowledge.
MGM and MGM-2 are deterministic both in terms of actions and environment, with cooperative group behaviour sharing incomplete (local) knowledge.

DCOP algorithms are used for several applications, for instance:
\begin{itemize}
    \item Disaster management
    \item Radio frequency allocation problems
    \item Recommendation Systems
    \item Scheduling
    \item Sensor networks
    \item Service-oriented management (cloud, server, power supply)
    \item Supply chain management
\end{itemize}

 
