// Copyright (C) Maxime MORGE 2020
package org.scadcop.util

import org.scadcop.problem._
import org.scadcop.solver.decentralized._
import akka.actor.ActorSystem

/**
  * Main application to test ScaDCOP on a toy example
  */
object Main extends App {
  //import org.scadcop.example.RobeColouringExample._
  import org.scadcop.example.DalekSurveillanceSystem._
  //or import org.scadcop.example.ToyExample._

  val system = ActorSystem("TestDistributedSolver")

    if (! pb.isSound) throw new RuntimeException("Pb is not sound")
    println(pb)

    println(s"First assignment: $initialContext")
    if (! pb.isSound(initialContext))
      throw new RuntimeException("this assignment is not sound")
    if (! pb.isFull(initialContext))
      throw new RuntimeException("this assignment is not full")
    println("with objective: " + pb.objective(initialContext))

    val solver = new DecentralizedSolver(pb, MGM2, initialContext, system)
    solver.trace = true
    solver.debug = false
    val outcome = solver.run()
    system.terminate
    println(s"Outcome: $outcome")
    println(s"Outcome objective: ${pb.objective(outcome)}")
    println(s"Solver metrics: ${solver.metric}")
}
