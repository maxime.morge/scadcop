// Copyright (C) Maxime MORGE 2020
package org.scadcop.problem

import org.scadcop.util.MathUtils._
import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the dalek surveillance system
  */
class ObjectiveDalekSurveillanceSystemSpec extends AnyFlatSpec {

  import org.scadcop.example.DalekSurveillanceSystem._

  behavior of "the dalek surveillance system"

  "The objective when all the directions are north" should " be 33.0" in {
    println(pb)
    println(initialContext)
    assert(pb.objective(initialContext) ~= 33.00)
  }

  "The objective when all the directions are south" should " be 33.0" in {
    println(pb)
    val assignmentSouth : Context = new Context().fix(
      Map(d1-> south, d2 -> south, d3 -> south, d4 -> south, d5 -> south))
    println(assignmentSouth)
    assert(pb.objective(assignmentSouth) ~= 33.00)
  }



}
