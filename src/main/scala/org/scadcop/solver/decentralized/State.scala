// Copyright (C) Maxime MORGE 2020
package org.scadcop.solver.decentralized

/**
  * Trait representing a state of the behaviour
  */
trait State
trait AgentState extends State