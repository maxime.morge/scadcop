// Copyright (C) Maxime MORGE, Alex VIGNERON 2020

package org.scadcop.solver.decentralized.mgm2

import org.scadcop.solver.decentralized._

/**
  * The variable agent behaviour is described by a Finite State Machine
  */
trait MGM2State extends AgentState
// The initial state in which the agent can be triggered by the supervisor
case object Init extends MGM2State
// the agent has either just started the algorithm or has finished a full round and got the Supervisor's KeepOn message
case object Continue extends MGM2State
//The agent waits for the role assignment
case object WaitingForRole extends MGM2State
// the offerer collects its neighbours
case object OffererWaitingValues extends MGM2State
// the offerer selects one neighbour
case object OffererMakingOff extends MGM2State
// the receiver gathers the neighbour's values
case object ReceiverWaitingValues extends MGM2State
// the receiver gathers the offers
case object ReceiverWaitingOffers extends MGM2State
// the receiver  selects the best offer
case object ReceiverAllOffersReceived extends MGM2State
// the receiver has not received yet any acceptable offer
case object Uncommitted extends MGM2State
// the receiver has accepted an offer
case object Committed extends MGM2State
//  the committed agent decides whether it is entitled to act in its own neighbourhood
case object GivingPartnerGoNogo extends MGM2State
// the committed agent which can act in its neighbourhood decides whether it should act according to its neighbour's message
case object HandlingPartnersGoNogo extends MGM2State
// the uncommitted agent acts according to MGM algorithm
case object ActSolo extends MGM2State
// the agent has received an End message by the Supervisor
case object Final extends MGM2State


/**
  * The supervisor agent behaviour is described by a Finite State Machine
  */
// the initial state in which the Supervisor can be triggered by the agent asking it to send the initial message
case object Start extends MGM2State
// the state where the supervisor stores values received from the agents
case object WaitingForAgentValues extends MGM2State
// the central decision state where the supervisor evaluates, given the current context, whether the whole process should stop or continue
case object DecidingToContinueOrStop extends MGM2State
// the final state the supervisor ends in after having stopped all other agents
case object Finish extends MGM2State


