// Copyright (C) Maxime MORGE 2019
package org.scadcop.solver.decentralized

/**
  * A metric for evaluating a [[DecentralizedSolver]]
  */
package object metric {
  type Metric = collection.mutable.Map[String,Double]
}
