// Copyright (C) Maxime MORGE 2020
package org.scadcop.solver.decentralized.supervisor

import org.scadcop.problem._
import org.scadcop.solver.decentralized._
import metric.Metric

/**
  * Immutable state of the supervisor
  * @param readyVariables which have the directory and are ready to start
  * @param inactiveVariables which are inactive
  * @param assignment i.e the current context
  * @param metric to evaluate the [[DecentralizedSolver]]
  * @param isFirstRound is true if the supervisor in in the first round
  */
class SupervisorStatus(val readyVariables: Set[Variable] = Set(),
                       val inactiveVariables: Set[Variable] = Set(),
                       val killedVariables: Set[Variable] = Set(),
                       val assignment: Context = new Context(),
                       val metric: Metric= collection.mutable.Map[String,Double](),
                       val previousObj: Double = 0.0,
                       val isFirstRound : Boolean = true
                      ){

  /**
    * Returns the number of ready agents
    */
  def nbReady : Int = readyVariables.size

  /**
    * Returns the number of inactive agents
    */
  def nbInactive : Int = inactiveVariables.size

  /**
    * Returns the number of killed agents
    */
  def nbKilled : Int = killedVariables.size

  /**
    * Returns true if the algorithm ends
    * @todo modify it
    */
  def isTerminated(newObjectiveValue : Double) : Boolean = newObjectiveValue <= previousObj

  /*Returns the updated supervisor's status when resetting the context*/
  def resetContext(): SupervisorStatus = {
    new SupervisorStatus(readyVariables,
      inactiveVariables = Set(),
      killedVariables,
      assignment = new Context(),
      metric,
      previousObj,
      isFirstRound
    )
  }

  //Returns the updated status with new value of objective function
  def updateObjectiveValue(newObj: Double): SupervisorStatus = {
    new SupervisorStatus(readyVariables, 
    inactiveVariables,
    killedVariables,
    assignment,
    metric,
    previousObj = newObj,
    isFirstRound
    )
  }
}


