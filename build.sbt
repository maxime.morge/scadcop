name := "ScaDCOP"

version := "0.1"

scalacOptions += "-deprecation"
scalacOptions += "-feature"

javaOptions in Compile += "-Xms4G"
javaOptions in Compile += "-Xss2M"
javaOptions in Compile += "-Xmx4G"
javaOptions in Compile += "-Dfile.encoding=UTF-8"

mainClass in (Compile,run) := Some("org.scadcop.util.Main")

fork := true

cancelable in Global := true

resolvers += ("Artima Maven Repository" at "http://repo.artima.com/releases").withAllowInsecureProtocol(true)
resolvers += ("Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/").withAllowInsecureProtocol(true)

scalaVersion := "2.13.2"


libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.6.3",
  "com.typesafe.akka" %% "akka-remote" % "2.6.3",
  "org.scalactic" %% "scalactic" % "3.1.1",
  "org.scalatest" %% "scalatest" % "3.1.1" % "test",
  "io.github.cquiroz" %% "scala-java-time" % "2.0.0"
)

logBuffered in Test := false
trapExit := false