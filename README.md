# ScaDCOP
ScaDCOP is a Scala library of algorithm for Distributed Constraints Optimization Problems

## What is ScaDCOP ?

ScaDCOP is a Scala library of algorithm for Distributed Constraints
Optimization Problems. We have implemented our prototype with the
[Scala](https://www.scala-lang.org/) programming language and the
[Akka](http://akka.io/) toolkit. The latter, which is based on the
actor model, allows us to fill the gap between the specification and
its implementation.


## Requirements

In order to run the [demonstration](src/main/scala/org/scadcop/util/Main.scala) you need:

- the Java virtual machine [JVM 1.13.0_02](http://www.oracle.com/technetwork/java/javase/downloads/index.html);

- the component [PlantUML](https://plantuml.com) to draw the interaction diagramms. 

In order to compile the code you need:

- the programming language [Scala 2.13.2](http://www.scala-lang.org/download/);

- the interactive build tool [SBT 1.3.9](http://www.scala-sbt.org/download.html).


## Installation

Compile

    sbt compile

then

    sbt run
        

## Example 

We consider the following example of graph-colouring [example](src/main/scala/org/scadcop/example/RobeColouringExample.scala):

```Scala
  val white : NominalValue = NominalValue("white")
  val black : NominalValue = NominalValue("black")
  val colours = List(black, white)
  val gandalf = new Variable(name = "gandalf", colours)
  val aragorn = new Variable(name = "aragorn", colours)
  val frodo = new Variable(name = "frodo", colours)

  val costGandalfAragorn : Array[Array[Double]] =
    Array(Array(3.0, 0.0), Array(8.0, 5.0))
  val costGandalfFrodo : Array[Array[Double]] =
    Array(Array(1.0, 0.0), Array(5.0, 6.0))
  val costFrodoGandalf : Array[Array[Double]] =
    Array(Array(4.0, 0.0), Array(3.0, 1.0))

  val constraintGandalfAragorn = new Constraint(gandalf, aragorn, costGandalfAragorn)
  val constraintGandalfFrodo = new Constraint(gandalf, frodo, costGandalfFrodo)
  val constraintFrodoGandalf = new Constraint(frodo, aragorn, costFrodoGandalf)
  val pb = new DCOP(Set(gandalf, aragorn, frodo), Set(constraintGandalfAragorn, constraintGandalfFrodo, constraintFrodoGandalf))
```

## Algorithm

The MGM2 [[1]](#1) algorithm is implemented in a multiagent approach.  

The [behaviour](src/main/scala/org/scadcop/solver/decentralized/mgm2/AgentBehaviour.scala) of the [agent](src/main/scala/org/scadcop/solver/decentralized/agent/VariableAgent.scala) representing a [variable](src/main/scala/org/scadcop/problem/Variable.scala) is based on :
- a static knowledge of
    - the variable it is responsible for,
    - its neighbours, i.e the variables which are linked to its variables,
    - the constraints over its variable;
- a dynamic [state of mind](src/main/scala/org/scadcop/solver/decentralized/mgm2/MGM2Mind.scala) (a context, delta, etc.);
- a [state](src/main/scala/org/scadcop/solver/decentralized/mgm2/MGM2State.scala) of a finite automaton.

These agents exchange some [mesages](src/main/scala/org/scadcop/solver/decentralized/mgm2/MGM2Message.scala) between them and some other [mesages](src/main/scala/org/scadcop/solver/decentralized/Message.scala) with the
[supervisor](src/main/scala/org/scadcop/solver/decentralized/supervisor/Supervisor.scala) which triggers and stop them. The behaviour of the latter is based on:
- some static information (the DCOP problem);
- a dynamic [status](src/main/scala/org/scadcop/solver/decentralized/supervisor/SupervisorStatus.scala);
- a [state](src/main/scala/org/scadcop/solver/decentralized/supervisor/SupervisorState.scala) of a finite automaton.

During a round, each agent :
1. computes a random value for its variable;
2. informs its neighbours about this value;
3. informs the supervisor.

The solving performs two rounds.

![Interaction diagramm](trace.png)

## Contributors

Copyright (C) Maxime MORGE, Alex VIGNERON 2020

## References
<a id="1">[1]</a> 
Maheswaran R.T., Pearce J.P., Tambe M. (2006)
A Family of Graphical-Game-Based Algorithms for Distributed Constraint Optimization Problems. 
In: Scerri P., Vincent R., Mailler R. (eds)
Coordination of Large-Scale Multiagent Systems. Springer, Boston, MA.

## License

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
