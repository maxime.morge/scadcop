// Copyright (C) Maxime MORGE, Alex VIGNERON 2020
package org.scadcop.solver.decentralized.mgm2

import org.scadcop.problem._
import org.scadcop.solver.decentralized._

// Message initially sent by the agent when it is triggered to ask the supervisor for a Continue messages
case object KickStartMe extends Message

// Choose a subset of neighbours as potential offerers/receivers
case class Choose(variables : Set[Variable]) extends Message

// Informs itself of its identity as offerer (T) or receiver (F)
case class ChooseSubset(isOfferer : Boolean) extends Message

// Inform the neighbours about delta
case class InformDelta(delta: Double) extends Message

// Inform the neighbours about the current value
// already defined as a org.scadcop.solver.decentralized.Message

// Propose eventually an offer
case class MakeOffer(offer : Offer) extends Message

// Accept a Move
case class Accept(offer : Offer) extends Message

// Reject an offer
case class Reject(offer : Offer) extends Message

// Identify the best offer
case class BestOffer(offer : Option[Offer]) extends Message

// Ask the agent to act or not
case class Act(confirmation: Boolean, move: Option[Move]) extends Message

// Tell the agent's partner that it can proceed
case class GiveGo(move: Move) extends Message

// Tell the agent's partner that it should not proceed
case object GiveNoGo extends Message

// supervisor tells agent to stop
case object StopAlgo extends Message

// supervisor tells agent what to continue
case object ContinueAlgo extends Message

// The supervisor tells the agent to start the full process and enter the first
//case object BeginAlgorithm extends Message the message
// Trigger is already defined as a org.scadcop.solver.decentralized.Message