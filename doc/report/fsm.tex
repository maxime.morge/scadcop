\section{FSM modeling}

\subsection{Outline}
According to the multi-agent approach, values of variables are discussed among agents who come to local agreements and iteratively repeat the process to finally yield a satisfying global assignment.
Agents systematically base their decision on their local knowledge.

The process is triggered by a supervisor agent which invites other agents to begin their rounds of interactions.
Each agent $a_i$ is sent a \beginAlgo{} message by the supervisor and initiates its variable's value at random in the variable's domain.
The behaviour of each agent is defined by a finite state automata (see Figure~\ref{full_agent} in annex for the big picture and Figures~\ref{beginning}, \ref{offerer}, \ref{receiver}, \ref{committed} and \ref{uncommitted} for focused extracts ).

Communication of potential moves and their corresponding potential gains helps agents determine who should act. A single move is an action wherevy an agent changes the current value of its variable to another possible value belonging to the variable's domain.
A coordinated move is similar but involves two agents changing the values of their variables. Note that a move can also be static if the change to \textit{another} value of the domain happens to be the current value of the variable.

\subsection{Agent States}

\subsubsection{General overview}
\begin{itemize}
    \item \init{} : initial state in which the agent can be triggered by the supervisor.
    \item \continue{}: central state which sparks each round of the algorithm, agent's behaviour depends here on the message it receives from supervisor.
    \item \waitingForRole{}: agent awaits for the role assignment which will tell it whether it will be an Offerer or a Receiver for this round.
    \item The Offerer's side:
    \begin{itemize}
        \item \offWaitingValues{}: the offerer agent collects all of its neighbours values before it can choose a neighbour at random and select a joint offer for it.
        \item \offMakingOff{}: the Offerer selects one neighbour and selects an offer with that neighbour's value which is then sent. No matter the response given by the selected partner, only one offer can be made, hence if rejected the offerer will move on to act on its own.
    \end{itemize}
    \item The Receiver's side:
    \begin{itemize}
        \item \recWaitingOffers{}: the receiver agent gathers all offers he receives
        \item \recHavingRecAllOff{}: the Receiver has received all offers from its neighbourhood and now chooses the best offer among those.
    \end{itemize}
    \item \uncommitted{}: receivers who have not received any acceptable offer (or no offer at all) and Offerer's whose offer has been rejected join back in this state and wait for their neighbours deltas while computing their own deltas and informing their neighbourhood.
    \item \committed{}: offerers which offers have been accepted and Receiver which have received at least one acceptable offer become committed with their respective partners.
    \item \actSolo{}: uncommited agents evaluate their neighbourhood and decide whether they should act or not depending on the neighbourhood's winner.
    \item \givingPartnerGNG{}: committed agents evaluate their neighbourhood and decide whether they should act or not depending on the neighbourhood's winner.
    \item \handlingGNG{}: committed agents which can act check whether their partner can act too.
    \item \final{}: the last state an agent can be into, after having received the \stopAlgo{} message from the supervisor.
    \end{itemize}





    \begin{figure}
        \center{\includegraphics[width=\textwidth]{figures/fsm/agent_beginning.png}}
        \caption{Agent's beginning}
        \label{beginning}
    \end{figure}


\subsubsection{\init}
This is the initial state agents wait in before starting the algorithm.
Messages received in this state are: \begin{itemize}
\item \informVal{someVal}: if the agent receives a \informVal{someVal} message, it stashes it to deal with it when leaving \waitingForRole{}.

\item \beginAlgo{}: upon receiving a \beginAlgo, sent by the supervisor,
  % 
agent performs a \initRandomVal{\val.domain} operation, this me\-thod is called only once per algorithm execution for each agent.
%
The agent unstashes all previous \informVal{} stashed messages and sends a \kickStart{} message to the Supervisor.
%
The agent will then transition to \continue{}.

During \initRandomVal{\val.domain}, the agent's variable $x_i$ is initialised at a random value $v_i \in d_i$.
When the Supervisor received \kickStart, it will send back a \continueAlgo{} message needed to get going.
This artificial trigger of \continueAlgo{} is used only once, at the begining of the algorithm.
\end{itemize}



\subsubsection{\continue{}}
This state can be reached either via \init, \givingPartnerGNG{} or \handlingGNG{}, it should be considered as the central state from which is sparked each round.
Messages received in this state: \begin{itemize}
    \item \informVal{someVal}: neighbour's \informVal{someVal} are stashed to be dealt with when leaving \waitingForRole{}.
    \item \giveGo{}: If the agent has reached this state after completing a round where it had been committed but could not act because it did not win the neighbourhood's delta competition, it will receive its partners' \giveGo{} or \giveNoGo{} here, but nothing should be done about it.
    \item \giveNoGo{}: If the agent has reached this state after completing a round where it had been committed but could not act because it did not win the neighbourhood's delta competition, it will receive its partners' \giveGo{} or \giveNoGo{} here, but nothing should be done about it.
    \item \stopAlgo{}: agent transitions to \final{} with the whole process terminating
    \item \continueAlgo{}: agent called  \reset{} method, emptying\linebreak \receivedOffers{}, \neighbourValues{} and \neighbourDeltas{}.
      early received \informVal{} messages are unstashed, then the agent sends its own value via \informVal{\val} to all its neighbours. It performs \determineSubset{} and send the outcome  to self via \linebreak \chooseSubset{} before  transitioning to \waitingForRole{} where  \chooseSubset{} will be dealt with as well as unstashed \informVal{}.
    \item \reject{o}: stashes it.
    \item \makeOff{o}: stashes it.
    \item \informDelta{d}: stashes it.
    \end{itemize}


\subsubsection{\waitingForRole{}}
This state acts as a crossroads between paths as Offerer or Receiver.
Messages received in this state: \begin{itemize}
    \item \informVal{someVal}: are stashed to be dealt with when leaving this state for the targeted next one.
    \item \chooseSubset{someSubset}: triggers a transition to either\linebreak \offWaitingValues{} or \recWaitingOffers{} according to the value of \textit{someSubset}.
\end{itemize}

\begin{figure}
    \center{\includegraphics[width=\textwidth]{figures/fsm/offerer.png}}
    \caption{Offerer agent}
    \label{offerer}
\end{figure}

\subsubsection{\offWaitingValues{}}
Agents reaching this state are considered offerers.
Messages received in this state: \begin{itemize}
    \item \informVal{someVal}: it is added to \neighbourValues{}'s map while some are missing, when the last message of the kind is received, it triggers the transitioning steps towards \offMakingOff.
    Once \linebreak \neighbourValues{}'s map is complete (i.e. all neighbours have informed the agent of their value), the agent will transition to\linebreak \offMakingOff{}. 
    \makeOff{myOffer} is sent to a neighbour chosen at random to be its potential partner; all other neighbours receive \makeOff{None}.
    \item \makeOff{someOffer}: all such messages will be answered with a \reject{} since Offerers can not be Receivers.
\end{itemize}


\subsubsection{\offMakingOff{}}
Before transitioning to the next state, the agent will wait here for the potential partner's response.
Messages received in this state: \begin{itemize}
    \item \informDelta{}: stashes it.
    \item \makeOff{someOffer}: message is still answered with a \reject.
    \item \informDelta{someDelta}: messages received from neighbours which are one step ahead are stashed at this stage.
    \item \accept{someCombinedMove}: it will transition to committed and set \commitment{} to true as well as set \partner{} to \linebreak \accept{someOffer}'s sender, this data will also help to compute the new delta.\\
    \informDelta{\neighbourDeltas.get(self.variable)} is sent to all neighbours after having computed its new delta either through it's partner's \linebreak \accept{someCombinedMove}'s delta or on its own if it has been rejected with \computeSoloDelta{}.
    \item \reject{someOffer}: the agent will transition to \uncommitted{}, set\linebreak \commitment{} to true and set \partner{} to \texttt{None}.
    \\ \informDelta{\neighbourDeltas.get(self.variable)} is sent to all neighbours after having computed its new delta either through it's partner's\linebreak \accept{someCombinedMove}'s delta or on its own if it has been rejected with  \computeSoloDelta{}.
\end{itemize}

\begin{figure}
    \center{\includegraphics[width=\textwidth]{figures/fsm/receiver.png}}
    \caption{Receiver agent}
    \label{receiver}
\end{figure}

\subsubsection{\recWaitingValues{}}
In this state the receiver agent waits until it has been informed of all its neighbours' values.
Messages received in this state: \begin{itemize}
    \item \informDelta{}: stashes it.
    \item \informVal{someVal}: are stored in the \neighbourValues{} map and become relevant only if the receiver does not get any acceptable offer.
        Once all of them have been received, the agent transitions to \recWaitingOffers{}.
    \item \makeOff{None/someOffer}: are stashed to be handled in\linebreak \recWaitingOffers{}.
\end{itemize}  

\subsubsection{\recWaitingOffers{}}
The last offer to be received causes the transition to the next state.
At the end, when the last offer is received, whether the agent has received a real or \texttt{None} last offer, it will process the list of real offers received with \linebreak \determineBestOffer{}.\\
Messages received in this state: \begin{itemize}
    \item \makeOff{someOffer}: stored in \receivedOffers{} map until all offers have been received. 
    \item \makeOff{None}: a counter is simply increased to keep track of the number and know when all offers ahev been received.
\end{itemize}
Once all offers have been received, the agent computes the result of\linebreak \determineBestOffer{} and \linebreak sends it to itself with \bestOfferFM{someOffer} which will be handled in the next state \recHavingRecAllOff{}.


\subsubsection{\recHavingRecAllOff{}}
This state is a fork between committed and uncommitted Receiver agents.
Messages received in this state: \begin{itemize}
    \item \informDelta{someDelta}: will be stashed and handled in either \committed{} or \uncommitted{}.
    \item \bestOfferFM{someOffer}: will cause the agent to transition to \committed{}, setting \commitment{} to \texttt{true} and setting its \partner{} variable to the agent it has committed with.
    the agent's current delta value is computed with \computeJointDelta{somePartner}.
        It will send a \reject{} to all neighbours which are not the partner and an\linebreak \accept{someCombinedMove} to the chosen partner.
    \item \bestOfferFM{None}: will cause the agent to transition to \uncommitted{} and set its \commitment{} to \texttt{false}. 
    It will send a \reject{} to all neighbours. The agent's current delta value is computed with \linebreak\computeSoloDelta{} and added to \linebreak\neighbourDeltas.
\end{itemize}
In both \bestOfferFM{None/someOffer} cases, \informDelta{someDelta} is sent to all neighbours, containing either the result of \linebreak\computeJointDelta{someDelta} or \linebreak\computeSoloDelta{}.

\begin{figure}
    \center{\includegraphics[height=\textheight]{figures/fsm/committed.png}}
    \caption{Committed agent}
    \label{committed}
\end{figure}

\subsubsection{\committed{}}
This state can be reached from \recHavingRecAllOff{} or \linebreak \offMakingOff{}.
Messages received in this state: \begin{itemize}
    \item \giveGo{}: stashes it.
    \item \informDelta{someDelta}: messages are added to \neighbourDeltas{} until they have been received from all neighbours.
        When all deltas have been received, the best delta of the neighbourhood can be computed.
        Once it is computed, the agent will send itself either \shouldAct{true} or \shouldAct{false} depending on whether it has the highest delta of its neighbourhood or not.
\end{itemize}

\begin{figure}
    \center{\includegraphics[width=\textwidth]{figures/fsm/uncommitted.png}}
    \caption{Uncommitted agent}
    \label{uncommitted}
\end{figure}

\subsubsection{\uncommitted{}}
Similarly to \committed{}, this state can be reached either from\linebreak \recHavingRecAllOff{} or \offMakingOff{}.
Messages received in this state: \begin{itemize}
    \item \informDelta{someDelta}: are added to \neighbourDeltas{} until it they have been received from all neighbours.
    Once all deltas have been received, it will compute whether it should act or not based on neighbourhood deltas and send either send \shouldAct{true} or \shouldAct{false} and transition to \actSolo{}. 
\end{itemize}


\subsubsection{\actSolo{}}
Here the uncommitted agent processes the \shouldAct{true/false} message, it will transition to \continue{} either way.
Messages received in this state: \begin{itemize}
    \item \shouldAct{true}: \val{} will be updated to \computeNewVal{}, inform supervisor of current value with \informVal{\val}.
    \item \shouldAct{false}: nothing except inform supervisor of current value with \linebreak\informVal{\val}.
\end{itemize}


\subsubsection{\givingPartnerGNG{}}
We postulate that all agents adopt the same behaviour, they speak before they listen.
Hence the agent migh receive its partner's \giveGo{} or \giveNoGo{} message which will be stashed and handled in \handlingGNG{}.
In any case, the agent handles the self-sent message \shouldAct{true/false}.
Messages received in this state: \begin{itemize}
    \item \shouldAct{false}: it will send its partner a \giveNoGo{} signal and transition to \continue{} by sending the Supervisor the \informVal{\val}.
    \item \shouldAct{true}: the agent sends its partner a \giveGo{} and transitions to \linebreak\handlingGNG{}.
\end{itemize}


\subsubsection{\handlingGNG{}}
Here the agent is the best among its neighbours and will check whether its partner has a similar situation.
No matter the message received, both will lead to transitioning to \continue{}, however othe actions taken to transition differ.
Messages received in this state: \begin{itemize}
    \item \giveGo{}: the potentially stashed \giveGo{} and \giveNoGo{} messages are handled.
        If the agent got a \giveGo{} from its partner, it will update its value \computeNewVal{} and inform the Supervisor of it
    \item \giveNoGo{}: the potentially stashed \giveGo{} and \giveNoGo{} messages are handled.
        If the agent got a \giveNoGo{} from its partner, it won't update its value and simply transition to \continue{}.
        The agent will not update its value
\end{itemize}


\subsubsection{\final{}}
This state is reached coming from \continue{} in the case where the Supervisor sent \stopAlgo{}.
No messages are sent nor received here, it merely represents the end of the algorithm.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Agent's Mind}
The agent's mental state can be considered as the variables which hold the agent's perception of its surroundings and of its inner state.
In our case, each agent possesses several \textit{personal} beliefs and several other \textit{interpersonal}.

\subsubsection{Personal beliefs}
\begin{itemize}
    \item \neighbourValues{} : is the current beliefs about variables in the vicinity including its own variable.
    \item \commitment{}: is a boolean indicating whether an agent is committed or not, makes the variable \partner{} relevant.
    \item \partner{}: is the neighbouring agent with which the agent is committed, only relevant when committed is set to true.
    \item \receivedOffers{}: are the offers receives by the agent during this turn, potentially empty ones.
    \item \nbReceivedOffers{}: is the number of all offers received in the current turn.
    \item \neighbourDeltas{}: is a map containing the agent's and all its neighbours' current deltas.
    \item \currentBestOffer{} : is the potential best offer the agent has, eventually none.
\end{itemize}


\subsection{Messages}
In order to carry out negotiations, exchange information and coordiante themselves, agents can opt for a variety of messages.
Each message can only be handled in dedicated states and will therefore be stashed to be processed later on.
\subsubsection{Standard agent messages}
\begin{itemize}
    \item \kickStart{}: is sent by the agent at the very begining of the first round of the process to indicate to the Supervisor that it needs to send it the initial \continueAlgo{} message.
    \item \chooseSubset{Offerer/Receiver}: is sent by the agent to itself, sent in the \waitingForRole{} state and then causing the transition towards either \offWaitingValues{} or \recWaitingOffers{}. This message determines the path the agent threads for the next two states before joigning again in the \uncommitted{}/\uncommitted{} states.
    \item \informVal{someVal}: is sent by an agent to all neighbouring agents in order to inform them about the current value of the variable controlled by the sender agent.
    \item \informDelta{\neighbourDeltas.get(self.variable)}: is sent by an agent which has computed its solo or joint delta to its neighbourhood upon transitioning to either \uncommitted{} or \uncommitted{} states.
    \item \makeOff{someOffer/None}: is sent by an offerer.
        An offer contains all coordinated moves between the offerer and the designated receiver with their respective delta from the offerer's perspective.
        A \textit{None} offer is sent by an offerer to all receivers which have not been chosen by it for this round.
    \item \reject{}: is sent by a receiver to all the non-chosen offerers.
    It can also be sent by an offerer if it has been made an offer, in this case no computation is needed since the offer is declined straight away.
    \item \accept{someCoordinatedMove}: is sent by a receiver to the best offerer upon having computed the best offer from all the received ones.
    It contains a couple consisting of the desired coordinated move and the delta achieved by it.
    \item \bestOfferFM{someOffer}: is sent by the receiver agent to itself after having received all offers from its neighbourhood.
        This message contains the offer which must be selected.
    \item \shouldAct{}: is sent by an agent, either offerer or receiver, it is sent when transitioning either from \uncommitted{} or \uncommitted{} to \givingPartnerGNG{}.
    \item \giveGo{}: is sent by the partner which has received a \shouldAct{true}.
    \item \giveNoGo{}: is sent by the partner which has received a \shouldAct{false}.
    \item \stopAlgo{}: is sent by the supervisor to inform agents that they should do another full cycle.
    \item \continueAlgo{}: is sent by the supervisor to the agents to inform them that they should stop.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Outline}
The supervisor has a simple behaviour based on mainly two states. 
In one of them, it awaits for all the agent's values.
Once all values have been collected, it switches to the second state, evaluating whether the full process had ended or not.
From there, it will branch, either back to the other state or to the final one once the condition for ending is reached.

\subsection{Supervisor states}

\subsubsection{General overview}
\begin{itemize}
    \item \waitingAgentValues{}: the main state where supervisor waits for agents' information.
    \item \startState{}: the state in which the supervisor starts, from there it broadcasts the \trigger message to all agents and directly goes to \linebreak\waitingAgentValues.
    \item \decidingStopContinue{}: state in which the supervisor checks whether the stopping condition has been met or not.
    \item \finishState{}: the final state the supervisor ends in after the full algorithm has ended and the stopping condition is met.
\end{itemize}

%\begin{figure}
%    \center{\includegraphics[width=\textwidth]{figures/fsm/mgm2_supervisor.png}}
%    \caption{Supervisor agent}
 %   \label{supervisor}
%\end{figure

\subsubsection{\startState}
Transiant state where the supervisor doesn't stay, no messages can be received here.

\subsubsection{\waitingAgentValues}
This is the main state the supervisor spends most time in. Here it collects values from agents in order to make its decision.
Messages received in this state: \begin{itemize}
    \item \kickStart{}: this is directly answered with a \continueAlgo{} message.
    \item \informVal{someVal}: these messages are put into the supervisors' \linebreak\currentContext until it is filled.
    The last one triggers a transition to \decidingStopContinue{}.
\end{itemize}

\subsubsection{\decidingStopContinue}
Here the supervisor either continues or stops, according to the message it send itself before transitioning here from \waitingAgentValues{}.
Messages received in this state: \begin{itemize}
    \item \informVal{}: is stashed to be handled in \waitingAgentValues{}.
\end{itemize}

\subsubsection{\finishState}
Final state of the automaton, no messages are received here.


\subsection{Supervisor's mental state}
The supervisor only handles one mental state variable:\linebreak \currentContext{}.
In it it stores the current values all variables have.

\subsection{Supervisor messages}
\begin{itemize}
    \item \beginAlgo{}: is the initial message sent once by the supervisor to all agents to launch the process.
    \item \continueAlgo{}: is sent by the supervisor to indicate that another round shoudld be carried out.
    \item \stopAlgo{}: is sent by the supervisor when the halting condition has been met.
\end{itemize}

