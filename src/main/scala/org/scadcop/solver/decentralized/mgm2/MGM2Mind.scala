// Copyright (C) Maxime MORGE, Alex VIGNERON 2020
package org.scadcop.solver.decentralized.mgm2

import org.scadcop.problem._
import org.scadcop.solver.decentralized._
import org.scadcop.solver.decentralized.agent._
import org.scadcop.solver.decentralized.metric._
import org.scadcop.util.RandomUtils._
import org.scadcop.util.MathUtils._

import scala.collection.mutable

/**
  *  Class representing the state of mind
  *  for a [[AgentBehaviour]]
  *  @param context current partial assignment of the variable and the linked ones
  *  @param isCommitted true if the agent is committed with a partner
  *  @param partner is eventually the neighbour which the agent is committed with
  *  @param receivedOffers are offers received from neighbours, eventually empty ones
  *  @param deltas are the improvements found by the neighbours and me
  *  @param metric to evaluate the DecentralizedSolver
  */
class MGM2Mind(val context : Context = new Context(),
               val isCommitted : Boolean = false,
               val partner : Option[Variable] = None,
               val receivedOffers : List[Offer] = List.empty,
               val nbReceivedOffers : Int = 0,
               var deltas : Map[Variable, Double] = Map(),
               val metric: Metric= collection.mutable.Map[String,Double](),
               val currBestOffer : Option[Offer] = None
               )
              extends Mind(context) {

  /**
    * Resets all expected the current value of the variable
    */
  def reset(variable: Variable) : MGM2Mind =
    new MGM2Mind(
      new Context().fix(variable, context.getValue(variable) match {
        case Some(value) => value
        case None => variable.randomValue
      }),
  )

  /**
    * Chooses randomly a partner amongst the neighbours
    */
  def choosePartner(neighbours: Set[Variable]): Variable = random[Variable](neighbours)

  /**
    * Returns true if the agent should be an offerer
    */
  def determineSubset(threshold: Double): Boolean = randomBoolean(threshold)

  /**
    * Returns the relevant constraint between the variable and the neighbour
    * amongst the set of constraints
    */
  def relevantConstraint(constraints: Set[Constraint],
                         variable: Variable,
                         neighbour: Variable): Constraint =
    neighbour.relevantConstraints(variable.relevantConstraints(constraints)).head

  /**
    * Returns the best unilateral move for the agent in the MGM algorithm
    * with the delta
    * eventually none and
    * updates the deltaRecord of the agent
    * @param variable which should be valuated
    * @param constraints which should be valuated
    */
  def bestUnilateralMove(variable: Variable,
                         constraints: Set[Constraint]): (Option[UnilateralMove],Double) = {

    var bestMove: Option[UnilateralMove] = None // The best move which will be selected, eventually none
    var bestDelta: Double = 0.0 // the best difference between the local cost with the one in the current context
    val currentCost: Double = currentContext.cost(constraints)
    variable.domain.foreach { value =>
      val potentialContext: Context = context.fix(variable, value)
      val potentialCost =  potentialContext.cost(constraints)
      val delta = potentialCost - currentCost
      if (delta ~> bestDelta) {
        val move = new UnilateralMove(potentialContext, delta)
        bestDelta = delta
        bestMove = Some(move)
      }
    }
    (bestMove, bestDelta)
  }

  /**
    * Returns all the coordinated moves for the agent
    * @param variable    which should be valuated
    * @param neighbours  the variables which are linked to the variables
    * @param constraints which should be valuated
    */
  def computeJointOffer(variable: Variable, potentialPartner: Variable,
                          constraints: Set[Constraint]
                         ): List[BilateralMove] = {
    val currentCost: Double = currentContext.cost(constraints)
    var moves = List[BilateralMove]()
      potentialPartner.domain.foreach { partnerValue => // for each value of the neighbour
        variable.domain.foreach { myValue => // for each value of the local variable
          if (myValue != context.getValue(variable).get ||
            myValue != context.getValue(potentialPartner).get) { // If this is a move from the current context
            val potentialContext: Context = context.fix(variable, myValue).fix(potentialPartner, partnerValue)
            val potentialCost: Double = potentialContext.cost(constraints)
            val gain = potentialCost - currentCost
            val move = new BilateralMove(potentialContext, gain)
            moves ::= move
          }
        }
      }
    moves
  }

  /**
     * Returns the best offer with a single move amongst the receivedOffers and
     * updates the neighbourDeltas of the agent
     * @param receiverVariable which should be valuated
     * @param neighbours  the variables which are linked to the variables
     * @param constraints which should be valuated
     */
   def chooseBestSingleOffer(receiverVariable: Variable,
                             constraints: Set[Constraint]): (Option[Offer], Double) = {
     val currentCost = currentContext.cost(constraints)
     var bestOffer: Option[Offer] = None // The best offer which will be selected, eventually none
     var bestDelta: Double = 0.0 // the best difference between the global cost with the one the current context
     receivedOffers.foreach{ offer: Offer => //for each offer
       val offererVariable = offer.offererVariable
       offer.moves.foreach { move: Move => //for each coordinated move
         val receiverValue: Value = move.context.getValue(receiverVariable).get
         val offererValue: Value = move.context.getValue(offererVariable).get
         val constraint: Constraint = relevantConstraint(constraints, receiverVariable, offererVariable)
         val commonUtilityBeforeChange = constraint.cost(context)
         val potentialContext = context.fix(offererVariable, offererValue).fix(receiverVariable, receiverValue)
          val commonUtilityAfterChange = constraint.cost(potentialContext)
         val potentialCost =  potentialContext.cost(constraints)
         val sharedUtilityGain = commonUtilityAfterChange - commonUtilityBeforeChange
         //Subtract the difference in the link between the two agent so it is not counted twice
         val delta: Double = potentialCost - currentCost + move.payoff - sharedUtilityGain
         if (delta ~> bestDelta) {
           bestDelta = delta
           bestOffer = Some(new Offer(offererVariable, List(move)))
         }
       }
     }
     (bestOffer, bestDelta)
   }

  /**
    * Returns true if the best improvement has been found by the agent
    * representing the variable or it partner
    */
   def amIBest(variable: Variable): Boolean = {
     // Sort the map according to
     // 1 - the deltas in decreasing order
     // 2 - in case of ties the lexicographic order over the variable
     val sortedMap = mutable.LinkedHashMap(
       deltas.toSeq.sortWith((x, y) => (x._2 ~> y._2) || ((x._2 ~= y._2) && (x._1 < y._1))) :_*)
     sortedMap.keys.head  == variable || (partner.nonEmpty && sortedMap.keys.head  == partner.get)
   }


   /**
   * Returns the new value the agent's variable should take
   */
   def updateVal(variable: Variable, move: Move): Value = {
     move.context.getValue(variable) match {
       case Some(v) => v
       case None => throw new RuntimeException(s"Error $variable should have at least one move")
     }
   }

   /*
   * Adding a neighbour's value to the current context
   */
   def updateContext(variable: Variable, newNeighbour: Variable, newNeighboursValue: Value) : MGM2Mind = {
    new MGM2Mind( context = context.fix(newNeighbour, newNeighboursValue),
    isCommitted,
    partner,
    receivedOffers,
    nbReceivedOffers,
    deltas,
    metric,
    currBestOffer)
  }

   /*
   * Updates neighbours' delta
   */
  def updateDeltas(variable: Variable, delta: Double) : MGM2Mind = {
    val updatedDeltas = deltas.removed(variable) + (variable -> delta)
    new MGM2Mind(context,
    isCommitted,
    partner,
    receivedOffers,
    nbReceivedOffers,
    updatedDeltas,
    metric,
    currBestOffer)
  }

   def resetNbOffersReceived(): MGM2Mind =
    new MGM2Mind(context,
    isCommitted,
    partner,
    receivedOffers,
    nbReceivedOffers = 0,
    deltas,
    metric,
    currBestOffer)

   /*
   * Add an offer
   */
   def addOffer(newOffer: Offer): MGM2Mind = {
     new MGM2Mind(context,
    isCommitted,
    partner,
    newOffer :: receivedOffers,
    nbReceivedOffers+1,
    deltas,
    metric,
    currBestOffer)
   }


   /**
   * Set partner
   */
   def setPartner(newPartner: Variable): MGM2Mind = {
    new MGM2Mind(context,
    isCommitted,
    Some(newPartner),
    receivedOffers,
    nbReceivedOffers,
    deltas,
    metric,
    currBestOffer)
  }

  /**
   * Set current best offer
   */
  def setCurrBestOffer(newOffer: Option[Offer]): MGM2Mind = {
    new MGM2Mind(context,
    isCommitted,
    partner,
    receivedOffers,
    nbReceivedOffers,
    deltas,
    metric,
    currBestOffer = newOffer)
  }

   /**
   * Set commitment
   */
  def uncommit(): MGM2Mind = {
    new MGM2Mind(context,
    isCommitted = false,
    partner = None,
    receivedOffers,
    nbReceivedOffers,
    deltas,
    metric,
    currBestOffer)
  }

  /**
   * Set commitment
   */
  def commitFully(newPartner: Variable, newOffer: Offer): MGM2Mind = {
    new MGM2Mind(context,
    isCommitted = true,
    partner =  Some(newPartner),
    receivedOffers,
    nbReceivedOffers,
    deltas,
    metric,
    currBestOffer = Some(newOffer))
  }

}
