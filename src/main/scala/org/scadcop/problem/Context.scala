// Copyright (C) Maxime MORGE 2020
package org.scadcop.problem

/**
  * Class representing a full assignment or a partial context
  * Each variable agent maintains a record of higher priority neighbors
  * @param valuation for some variables, eventually none
  */
class Context(val valuation: Map[Variable, Value] = Map()) {

  /**
    * String representation of the assignment
    */
  override def toString: String = valuation.mkString("[",", ","]")

  /**
    * Returns the number of instantiated variables
    */
  def size : Int = valuation.size

  /**
    * Returns the value for a variable
    */
  def getValue(variable: Variable) : Option[Value] = {
    if (valuation.contains(variable)) return Some(valuation(variable))
    None
  }

  /**
    * Returns a context by fixing the value of a variable
    */
  def fix(variable : Variable, value: Value) : Context = {
    if (! variable.domain.contains(value))
      throw new RuntimeException(s"$variable=$value is forbidden since $value is not in ${variable.domain}")
    new Context(valuation + (variable -> value))
  }

  /**
    * Fix the values of variables in the context
    */
  def fix(values :  Map[Variable, Value]) : Context ={
    var currentContext = this
    values.foreach{ case (variable, value) =>
      currentContext = currentContext.fix(variable,value)
    }
    currentContext
  }

  /**
    * Remove a variable in the context
    */
  def remove(variable: Variable) : Context= new Context(valuation - variable)

  /**
    * Returns true if the context is compatible with otherContext, i.e.
    * they do not disagree on any variable assignment
    */
  def isCompatible(otherContext : Context) : Boolean =
    this.valuation.keys.toSet.intersect(otherContext.valuation.keys.toSet).forall(variable =>
      this.valuation(variable) == otherContext.valuation(variable)
    )

  /**
    * Returns the cost of som constraints
    */
  def cost(constraints: Set[Constraint]): Double = {
    constraints.foldLeft(0.0)((sum, constraint) =>
      sum + constraint.cost(this)
    )
  }



}
