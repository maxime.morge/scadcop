// Copyright (C) Maxime MORGE 2020
package org.scadcop.solver.decentralized

/**
  * Class representing an algorithm
  */
class Algorithm{
  override def toString: String = this match {
    case MGM => "MGM"
    case MGM2 => "MGM2"
  }
}
case object MGM extends Algorithm
case object MGM2 extends Algorithm
