// Copyright (C) Maxime MORGE, Alex VIGNERON 2020
package org.scadcop.solver.decentralized.mgm2

import org.scadcop.problem._

/**
  * Abstract class representing a move
  * @param context is an assignment
  * @param payoff is the associated gain
  */
abstract class Move(val context : Context, val payoff: Double){
  override def toString: String = s"context=$context payoff= $payoff"
}

/**
  * Class representing an unilateral move
  * @param context is an assignment for the variable
  * @param localGain is the corresponding gain for the local cost of the agent
  */
class UnilateralMove(context : Context, localGain: Double)
  extends Move(context, localGain)

/**
  * Class representing a coordinated bilateral move
  * @param context is an assignment for the offerer's variable and
  *                the receiver's variable
  * @param offererGain is the offerer’s gain
  */
class BilateralMove(context : Context, offererGain: Double)
  extends Move(context, offererGain)
