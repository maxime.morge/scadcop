// Copyright (C) Maxime MORGE 2019
package org.scadcop.solver.decentralized.agent

import org.scadcop.problem._
import org.scadcop.solver.decentralized._

import akka.actor.{Actor, ActorRef}
import java.util.concurrent.ThreadLocalRandom
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Abstract class representing an agent
  * which manage a variable of a problem
  * with the following static information
  * @param variable which should be valuated
  * @param neighbours the variables which are linked to the variables
  * @param constraints  over the specific variable
  * */

abstract class VariableAgent(val variable: Variable,
                             val neighbours : Set[Variable],
                             val constraints: Set[Constraint]) extends Actor {
  var trace: Boolean = false
  var debug: Boolean = false
  var debugState: Boolean = false

  var supervisor: ActorRef = context.parent
  var directory: Directory = new Directory()
  val rnd : ThreadLocalRandom = ThreadLocalRandom.current()
  val deadline: FiniteDuration = 100 nanosecond

  /**
    * Multicast a message to some peers
    */
  def multicast(peers : Set[Variable], message : Message): Unit = {
    var isFirst = true
    var log = ""
    peers.foreach { other =>
      if (trace){
        if (isFirst) log += s"$variable-> $other : $message\n"
        else log += s"& $variable -> $other :\n"
      }
      isFirst = false
      directory.addressOf(other) ! message
    }
    if (trace) print(log)
  }

  /**
    * Broadcast a message to neighbours
    */
  def broadcast(message : Message): Unit = {
    multicast(neighbours, message)
  }
}
