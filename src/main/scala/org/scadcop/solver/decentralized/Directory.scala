// Copyright (C) Maxime MORGE 2020
package org.scadcop.solver.decentralized

import org.scadcop.problem.Variable

import akka.actor.ActorRef

/**
  * Class representing an index of the names and addresses of peers
  */
class Directory {
  var addressOf : Map[Variable, ActorRef] = Map[Variable, ActorRef]()//Variables' actors
  var variableOf : Map[ActorRef, Variable] = Map[ActorRef, Variable]()// Actors' variable

  /**
    * String representation
    */
  override def toString: String = allVariables().mkString("[",", ","]")

  /**
    * Add to the directory
    * @param variable to add
    * @param ref to add
    */
  def add(variable: Variable, ref: ActorRef) : Unit = {
    if ( ! addressOf.keySet.contains(variable) &&  ! variableOf.keySet.contains(ref)) {
      addressOf += (variable -> ref)
      variableOf += (ref -> variable)
    }
    else throw new RuntimeException(s"$variable and/or $ref already in the directory")
  }

  def allActors() : Iterable[ActorRef]  = addressOf.values
  def allVariables() : Iterable[Variable]  = variableOf.values
  def peers(variable: Variable) : Set[Variable] = allVariables().filterNot(_ ==variable).toSet
  def peersActor(variable: Variable) :  Iterable[ActorRef] = peers(variable: Variable).map(w => addressOf(w))
}

