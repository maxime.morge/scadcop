// Copyright (C) Maxime MORGE 2020
package org.scadcop.problem

import org.scadcop.util.MathUtils._

import org.scalatest.flatspec.AnyFlatSpec

/**
  * Unit test for the robe colouring example
  */
class ObjectiveRobeColouringExampleSpec extends AnyFlatSpec {

  import org.scadcop.example.RobeColouringExample._

  behavior of "the robe colouring example"

  "The objective when all the robes are black" should " be 8.0" in {
    println(pb)
    println(initialContext)
    assert(pb.objective(initialContext) ~= 8.00)
  }

  "The objective when all the robes are white" should " be 12.0" in {
    println(pb)
    val assignmentWhite : Context= new Context()
      .fix(Map(gandalf -> white, aragorn -> white, frodo -> white))
    println(assignmentWhite)
    println(assignmentWhite)
    assert(pb.objective(assignmentWhite) ~= 12.00)
  }



}
