// Copyright (C) Maxime MORGE 2020
package org.scadcop.example

import org.scadcop.problem._

import scala.beans.BooleanBeanProperty

/**
  * Object representing a toy DCOP instance
  */
object ToyExample extends{
  val f : BooleanValue = BooleanValue(false)
  val t : BooleanValue = BooleanValue(true)
  val booleanDomain = List(t, f)
  val x1 = new Variable(name = "x1", booleanDomain)
  val x2 = new Variable(name = "x2", booleanDomain)
  val x3 = new Variable(name = "x3", booleanDomain)
  val x4 = new Variable(name = "x4", booleanDomain)
  val cost : Array[Array[Double]] = Array(Array(1.0, 2.0), Array(2.0, 0.0))
  val c12 = new Constraint(x1, x2, cost)
  val c13 = new Constraint(x1, x3, cost)
  val c23 = new Constraint(x2, x3, cost)
  val c24 = new Constraint(x2, x4, cost)
  val pb = new DCOP(Set(x1, x2, x3, x4), Set(c24, c23, c13, c12))
  val assignment : Context = new Context().fix(Map(x1-> t, x2 -> t, x3 -> t, x4 -> t))
  val secondAssignment : Context = new Context().fix(Map(x1 -> f, x2 -> f, x3 -> f, x4 -> f))
}
