// Copyright (C) Maxime MORGE 2020
package org.scadcop.solver.decentralized

import org.scadcop.problem._
import metric.Metric

/**
  *  Messages between the actors
  */
abstract class Message

// MANAGING ACTOR
// Trace the messages
case object Trace extends Message
// Trigger the supervisor/agent
case object Trigger extends Message
// Returns back an assignment
case class Outcome(assignment : Context, metric: Metric) extends  Message
// Lights the agents about their acquaintances
case class Light(directory : Directory) extends Message
// Informs the supervisor that the agent is ready to start
case object Ready extends Message
// Informs the supervisor about the value selected by the agent
case class InformValue(value : Value) extends Message
// Informs the supervisor that the agent is active once again
case object Restart extends Message
// Query the agent about some metrics
case object Query extends Message
// Inform the supervisor about some metrics
case class Inform(metric: Metric) extends Message

// Specific message for the algorithm
case class SolverMessage() extends Message
