// Copyright (C) Maxime MORGE 2020
package org.scadcop.solver.decentralized


import org.scadcop.problem._
import org.scadcop.solver.Solver
import org.scadcop.solver.decentralized.supervisor.Supervisor
import metric.Metric

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Decentralized solver based on multi-agent approach
  * @param pb to be solver
  * @param algorithm which determines the agent behaviour
  * @param system of Actors
  */
class DecentralizedSolver(pb: DCOP, val algorithm: Algorithm, val initialContext: Context, val system: ActorSystem) extends Solver(pb) {

  var metric : Metric = collection.mutable.Map[String,Double]()

  override def name(): String = s"DecentralizedSolver($algorithm)"

  val TIMEOUTVALUE : FiniteDuration = 6000 minutes // Default timeout of a run
  implicit val timeout : Timeout = Timeout(TIMEOUTVALUE)

  // Launch a new supervisor
  DecentralizedSolver.id += 1
  val supervisor : ActorRef =
    system.actorOf(Props(classOf[Supervisor], pb, algorithm, initialContext),
      name = "supervisor"+DecentralizedSolver.id)

  /**
    * Returns an assignment
    */
  override def solve(): Context = {
    if (trace) supervisor ! Trace
    val future = supervisor ? Trigger
    val result = Await.result(future, timeout.duration).asInstanceOf[Outcome]
    if (debug) println(s"$name: solve ends")
    metric = result.metric
    result.assignment
  }
}

/**
  * Companion object for DecentralizedSolver
  */
object DecentralizedSolver{
  var id = 0
}
