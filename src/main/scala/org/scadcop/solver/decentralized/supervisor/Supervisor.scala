// Copyright (C) Maxime MORGE 2020
package org.scadcop.solver.decentralized.supervisor

import org.scadcop.problem._
import org.scadcop.solver.decentralized._
import org.scadcop.solver.decentralized.mgm2._
import akka.actor.{Actor, ActorRef, FSM, Props, Stash, PoisonPill}

/**
  * Supervisor which starts/stops the interaction between agents
  * @param pb DCOP instance to tackle
  * @param algorithm which determines the agent behaviour
  *
  * */
class Supervisor(val pb : DCOP, val algorithm: Algorithm, val initialContext: Context) extends Actor with Stash
    with FSM[SupervisorState, SupervisorStatus] {

  var debug = false
  var debugState = false
  var trace = false

  var solver: ActorRef = context.parent // Reference to the distributed solver
  var directory = new Directory() // White page for the peers

  /**
    * Initially non agent is ready, and the status is empty
    */
  startWith(InitialSupervisorState, new SupervisorStatus())

  /**
    * Method invoked after starting the actor
    */
  override def preStart(): Unit = {
    if (debug) println(s"Supervisor> ${pb.constraints}")
    pb.variables.foreach{ variable: Variable => //For each variable
      val neighbours  = pb.linkedVariablesOf(variable)
      val constraintsOfVariable = pb.constraintsOf(variable)
      // build the actor
      if (debug) println(s"Supervisor> variable=$variable constraints=$constraintsOfVariable")
      val actor = algorithm match {
          case MGM2  =>
            context.actorOf(Props(classOf[AgentBehaviour], variable, neighbours, constraintsOfVariable, initialContext), variable.name.toString)
          case other => throw new RuntimeException(s"Supervisor ($stateName): error the algorithm $other is not yet implemented")
        }
      directory.add(variable, actor)// add it to the directory
    }
  }

  /**
    * Handles message in the initial state
    */
  when(InitialSupervisorState) {
    //When the trace of the protocol is required
    case Event(Trace, status) =>
      trace = true
      if (trace) {
        println("'1- Download plantuml at https://plantuml.com")
        println("'2 -Save the trace in a file trace.txt")
        println("'3 - Run the following command line ")
        println("'java -jar plantuml.jar trace.txt")
        println("@startuml")
        println("skinparam monochrome true")
        println("skinparam handwritten true")
        println("hide footbox")
        println("participant Supervisor")
        for (variable <- pb.variables) println(s"participant $variable")
      }
      directory.allActors().foreach(_ ! Trace)
      stay using status
      
        //when it receives an InformValue from an early agent
    case Event(InformValue(_), status) => {
      stash
      stay using status
    }

    //When the supervisor is triggered
    case Event(Trigger, status) =>
      solver = sender
      directory.allActors().foreach { actor: ActorRef =>
        if (debug) println(s"Supervisor provides the directory to ${directory.variableOf(actor)}")
        actor ! Light(directory) // Wait for all the Ready messages to trigger the agent
      }
      stay using status

    //When an agent becomes ready
    case Event(Ready, status) if status.nbReady < pb.size - 1  =>
      val variable = directory.variableOf(sender)
      if (debug) println(s"Supervisor ($stateName): $variable is ready")
      stay using
        new SupervisorStatus(readyVariables =  status.readyVariables + variable, previousObj = status.previousObj,
          isFirstRound = status.isFirstRound)

    //When all the agents become ready
    case Event(Ready, status) if status.nbReady == pb.size - 1  =>
      val variable = directory.variableOf(sender)
      if (debug) println(s"Supervisor ($stateName): finally $variable is ready")
      pb.variables.foreach { anyVariable: Variable =>
        if (trace) println(s"Supervisor -> $anyVariable : Trigger")
        directory.addressOf(anyVariable) ! Trigger
      }
      goto(RunningSupervisorState) using
        new SupervisorStatus(readyVariables =  status.readyVariables + variable, previousObj = status.previousObj,
          isFirstRound = status.isFirstRound)
  }

  /**
    * Handles the message in the running state
    */
  when(RunningSupervisorState) {

    // When an agent wants to start
    case Event(KickStartMe, status) =>
      sender ! ContinueAlgo
      stay using status

    // When an agent becomes inactive
    case Event(InformValue(value), status) if status.assignment.valuation.size < pb.size - 1 =>
      val variable: Variable = directory.variableOf(sender)
      if (debug) println(s"Supervisor ($stateName): $variable is inactive")
      stay using new SupervisorStatus(readyVariables = status.readyVariables,
        inactiveVariables = status.inactiveVariables + variable,
        assignment = status.assignment.fix(variable,value),
        previousObj = status.previousObj,
        isFirstRound = status.isFirstRound)

    // When all the agent become inactive
    case Event(InformValue(value), status) if status.assignment.valuation.size == pb.size - 1 =>
      val variable: Variable = directory.variableOf(sender)
      if (debug) println(s"Supervisor ($stateName): $variable is finally inactive")
      var updatedStatus =  new SupervisorStatus(readyVariables = status.readyVariables,
        inactiveVariables = status.inactiveVariables + variable,
        assignment = status.assignment.fix(variable,value),
        previousObj = status.previousObj,
        isFirstRound = false)

      val newObjectiveValue = pb.objective(updatedStatus.assignment)

      if (! updatedStatus.isTerminated(newObjectiveValue)){// If the solving must be restart for another round
        pb.variables.foreach { anyVariable: Variable =>
          if (trace) println(s"Supervisor -> $anyVariable : ContinueAlgo")
          directory.addressOf(anyVariable) ! ContinueAlgo
        }
        updatedStatus = updatedStatus.updateObjectiveValue(newObjectiveValue)
        if (trace) println(s"Supervisor -> self : ContinueAlgo")
        self ! ContinueAlgo
      }

      else {// If the solving must be ended
        pb.variables.foreach { anyVariable: Variable =>
          if (trace && debug) println(s"Supervisor -> $anyVariable : Query")
          directory.addressOf(anyVariable) ! Query
        }
        if (trace) println(s"Supervisor -> self : StopAlgo")
        self ! StopAlgo
        }
        goto(Deciding) using updatedStatus
  }


  /**
    * Handles the messages when the supervisor is branching either to do anothe round or to stop.
    */
  when(Deciding) {

    case Event(Inform(measure), status) => {
      stash
      stay using status
    }

    //when an agent informs the supervisor of its current value
    case Event(InformValue(v), status) => {
      stash 
      stay using status
    }

    //when it receives its own decison that it should continue
    case Event(ContinueAlgo, status) => {
      println(s" ${Supervisor.incrementCounter()}/${Supervisor.NB_ROUNDS} ************************************************************************")
      val newStatus: SupervisorStatus = status.resetContext()
      unstashAll
      goto(RunningSupervisorState) using newStatus
    }

    case Event(StopAlgo, status) => {
      unstashAll
      goto(FinalSupervisorState) using status
    }

  }


  /**
    * Handles the message in the final state
    */
  when(FinalSupervisorState) {
    // When an agent returns back some metrics
    case Event(Inform(measure), status) if status.nbKilled < pb.size - 1 =>
      val variable: Variable = directory.variableOf(sender)
      measure.foreach{ case (k,v) =>
        status.metric.updateWith(k)({
          case Some(count) => Some(count + v)
          case None        => Some(v)
        })
      }
      stay using new SupervisorStatus(readyVariables = status.readyVariables,
        inactiveVariables = status.inactiveVariables,
        killedVariables = status.killedVariables + variable,
        assignment = status.assignment,
        status.metric,
        previousObj = status.previousObj,
        isFirstRound = status.isFirstRound)

    // When all the agent become inactive
    case Event(Inform(measure), status) if status.nbKilled == pb.size - 1 =>
      val variable: Variable = directory.variableOf(sender)
      val metric = status.metric
      measure.foreach{ case (k,v) =>
        metric(k) += v
      }
      pb.variables.foreach { anyVariable: Variable =>
        if (trace && debug) println(s"Supervisor -> $anyVariable : Kill")
        directory.addressOf(anyVariable) ! PoisonPill
      }
      if (trace) println("@enduml")
      solver ! Outcome(status.assignment, status.metric)
      stay using new SupervisorStatus(readyVariables = status.readyVariables,
        inactiveVariables = status.inactiveVariables,
        killedVariables = status.killedVariables + variable,
        assignment = status.assignment,
        metric, 
        previousObj = status.previousObj,
        isFirstRound = status.isFirstRound)
  }

    /**
    * Whatever the state is
    **/
  whenUnhandled {
    // If the supervisor must trace the protocol
    case Event(Trace, mind)  =>
      if (debug) println("Agent receives Trace")
      this.trace = true
      stay using mind
    case Event(msg @ _, status) =>
      throw new RuntimeException(s"ERROR: Supervisor in state $stateName receives the message $msg which was not expected")
      stay using status
  }

  /**
    * Allows to debug the transitions
    */
  onTransition {
    case InitialSupervisorState -> InitialSupervisorState =>
      if (debugState) println("Supervisor stays in InitialSolverState")
    case RunningSupervisorState -> RunningSupervisorState =>
      if (debugState) println("Supervisor stays in RunningSolverState")
    case InitialSupervisorState -> RunningSupervisorState =>
      if (debugState) println(s"Supervisor moves from InitialSolverState to RunningSolverState")
    case RunningSupervisorState -> InitialSupervisorState  =>
      if (debugState) println(s"Supervisor moves from RunningSolverState to InitialSolverState")
  }

  // Finally Triggering it up using initialize,
  // which performs the transition into the initial state and sets up timers (if required).
  initialize()
}

object Supervisor {
  var counter = 0
  val NB_ROUNDS = 10

  def incrementCounter() : Int = {
    counter = counter + 1
    counter
  }


}
