// Copyright (C) Maxime MORGE & Alex VIGNERON 2020
package org.scadcop.example

import org.scadcop.problem._

/**
  * Object representing the Dalek surveillance system problem
  */
object DalekSurveillanceSystem{
  val north : NominalValue = NominalValue("North")
  val east : NominalValue = NominalValue("East")
  val south : NominalValue = NominalValue("South")
  val west : NominalValue = NominalValue("West")
  val cardinalPoints = List(north, east, south, west)

  val d1 = new Variable(name = "d1", cardinalPoints)
  val d2 = new Variable(name = "d2", cardinalPoints)
  val d3 = new Variable(name = "d3", cardinalPoints)
  val d4 = new Variable(name = "d4", cardinalPoints)
  val d5 = new Variable(name = "d5", cardinalPoints)

  val cost12 : Array[Array[Double]] = Array(
    Array(10.0, 10.0, 5.0, 5.0),
    Array(5.0, 5.0, 1.0, 0.0),
    Array(5.0, 5.0, 1.0, 1.0),
    Array(10.0, 10.0, 5.0, 5.0))

  val cost13 : Array[Array[Double]] = Array(
    Array(5.0, 5.0, 5.0, 5.0),
    Array(0.0, 1.0, 1.0, 1.0),
    Array(1.0, 0.0, 1.0, 1.0),
    Array(5.0, 5.0, 5.0, 5.0))

  val cost14 : Array[Array[Double]] = Array(
    Array(5.0, 5.0, 10.0, 10.0),
    Array(1.0, 1.0, 5.0, 5.0),
    Array(0.0, 1.0, 5.0, 5.0),
    Array(5.0, 5.0, 10.0, 10.0))

  val cost23 : Array[Array[Double]] = Array(
    Array(5.0, 5.0, 5.0, 5.0),
    Array(5.0, 5.0, 5.0, 5.0),
    Array(1.0, 0.0, 1.0, 1.0),
    Array(0.0, 1.0, 1.0, 1.0))

  val cost25 : Array[Array[Double]] = Array(
    Array(5.0, 10.0, 10.0, 5.0),
    Array(5.0, 10.0, 10.0, 5.0),
    Array(0.0, 5.0, 5.0, 1.0),
    Array(1.0, 5.0, 5.0, 1.0))

  val cost34 : Array[Array[Double]] = Array(
    Array(1.0, 1.0, 5.0, 5.0),
    Array(1.0, 1.0, 5.0, 5.0),
    Array(1.0, 0.0, 5.0, 5.0),
    Array(0.0, 1.0, 5.0, 5.0))

  val cost35 : Array[Array[Double]]= Array(
    Array(1.0, 5.0, 5.0, 1.0),
    Array(0.0, 5.0, 5.0, 1.0),
    Array(1.0, 5.0, 5.0, 0.0),
    Array(1.0, 5.0, 5.0, 1.0))

  val cost45 : Array[Array[Double]]= Array(
    Array(1.0, 5.0, 5.0, 1.0),
    Array(1.0, 5.0, 5.0, 0.0),
    Array(5.0, 10.0, 10.0, 5.0),
    Array(5.0, 10.0, 10.0, 5.0))

  val constraint12 = new Constraint(d1, d2, cost12)
  val constraint13 = new Constraint(d1, d3, cost13)
  val constraint14 = new Constraint(d1, d4, cost14)
  val constraint23 = new Constraint(d2, d3, cost23)
  val constraint25 = new Constraint(d2, d5, cost25)
  val constraint34 = new Constraint(d3, d4, cost34)
  val constraint35 = new Constraint(d3, d5, cost35)
  val constraint45 = new Constraint(d4, d5, cost45)

  val pb = new DCOP(Set(d1, d2, d3, d4, d5), Set(constraint12, constraint13, constraint14, constraint23, constraint25,
        constraint34, constraint35, constraint45))

  val initialContext : Context = new Context().fix(
    Map(d1-> north, d2 -> north, d3 -> north, d4 -> north, d5 -> north))
}
