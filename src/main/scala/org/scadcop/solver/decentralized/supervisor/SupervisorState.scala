// Copyright (C) Maxime MORGE 2020
package org.scadcop.solver.decentralized.supervisor

import org.scadcop.solver.decentralized.State

/**
  * The supervisor behaviour is described by a Finite State Machine
  */
trait SupervisorState extends State
case object InitialSupervisorState extends SupervisorState
case object RunningSupervisorState extends SupervisorState
case object FinalSupervisorState extends SupervisorState
case object Deciding extends SupervisorState

