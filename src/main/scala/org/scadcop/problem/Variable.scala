// Copyright (C) Maxime MORGE 2020
package org.scadcop.problem

import org.scadcop.util._
import domain.Domain

/**
  * Class representing a variable
  * @param name is the id of the variable
  * @param domain is a finite and and discrete set of values
  */
class Variable(val name: String, val domain: Domain)extends Ordered[Variable]{

  /**
    * Short string representation of the variable
    */
  override def toString : String = name

  /**
    * Long string representation of the variable
    */
  def description : String = s"x$name in "+domain.mkString("[", ", ", "]")

  /**
    * Returns true if this variable is equal to that
    */
  override def equals(that: Any): Boolean =
    that match {
      case that: Variable => that.canEqual(this) && this.name == that.name
      case _ => false
    }
  def canEqual(a: Any) : Boolean = a.isInstanceOf[Variable]

  /**
    * Returns 0 if this an that are the same, negative if this < that, and positive otherwise
    * Variable are sorted with their name
    */
  def compare(that: Variable) : Int = {
    if (this.name == that.name) return 0
    else if (this.name > that.name) return 1
    -1
  }

  /**
    * Returns the index of a specific value
    */
  def index(value : Value) : Int = domain.indexOf(value)

  /**
    * Return true if the variable is sound, i.e. a non-empty domain
    */
  def sound() : Boolean = domain.nonEmpty

  /**
    * Returns a random value in the domain
    */
    def randomValue : Value =  RandomUtils.random(domain)

  /**
    * Returns the relevant constraints for the variable amongst the set of constraints
    */
  def relevantConstraints(constraints: Set[Constraint]) : Set[Constraint] =
    constraints.filter(c => c.varA == this || c.varB == this)

}