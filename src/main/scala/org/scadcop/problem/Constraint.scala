// Copyright (C) Maxime MORGE 2020
package org.scadcop.problem

/**
  * Class representing a binary constraint over two variables
  * which valuates the cost of each couple of values
 *
  * @param varA the first variable
  * @param varB the second variable
  * @param cost the cost matrix
  */
class Constraint(val varA: Variable, val varB: Variable, val cost: Array[Array[Double]]) {

  /**
    * String representation
    */
  override def toString: String = {
  var r = s"Cost(x${varA.name})(x${varB.name})\n"
    for {
      i <- varA.domain.indices
      j <- varB.domain.indices
    } r+=s"\t\tCost($i)($j) = ${cost(i)(j)}\n"
    r
  }

  /**
    * Returns true if the constraint is sound, i.e
    * the cost of each couple of values is known and positive
    */
  def sound() : Boolean = {
    if (this.cost.length != varA.domain.size) return false
    for {
      i <- varA.domain.indices
    } if (this.cost(i).length != varA.domain.size) return false
    for {
      i <- varA.domain.indices
      j <- varB.domain.indices
    } if (cost(i)(j) < 0.0) return false
  true
  }

  /**
    * Returns true if the constraint is over a specific variable
    */
  def isOver(variable: Variable) : Boolean = variable == varA || variable == varB

  /**
    * Returns the cost when
    * the variable var1 is assigned to the value val1
    * and the variable var2 is assigned to the value val2
    */
  def cost(var1: Variable, val1: Value, var2: Variable, vaB: Value): Double = {
    var (index1,index2) = (0,0)
    if (var1 == varA) {
      index1 = varA.index(val1)
      index2 = varB.index(vaB)
    } else if (var1 == varB) {
      index2 = varA.index(val1)
      index1 = varB.index(vaB)
    } else throw new RuntimeException(s"Constraint $this is not about $var1 and $var2")
    cost(index1)(index2)
  }

  /**
    * Returns the cost of the constraints for a context,
    * eventually throw an exception if one of the variable is not instantiated
    */
  def cost(context: Context): Double = {
    if (context.getValue(varA).isEmpty || context.getValue(varB).isEmpty)
      throw new RuntimeException(s"The cost of the constraint over $varA and $varB is not defined")
    val index1 = varA.index(context.getValue(varA).get)
    val index2 = varB.index(context.getValue(varB).get)
    cost(index1)(index2)
  }
}

