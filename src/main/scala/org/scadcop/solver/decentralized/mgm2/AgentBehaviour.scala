// Copyright (C) Maxime MORGE, Alex VIGNERON 2020
package org.scadcop.solver.decentralized.mgm2

import org.scadcop.problem._
import org.scadcop.solver.decentralized._
import org.scadcop.solver.decentralized.agent._

import akka.actor.{FSM, Stash}

/**
  * Negotiation behaviour for an agent in the MGM2 algorithm
  * See Maheswaran R.T., Pearce J.P., Tambe M. (2006)
  * A Family of Graphical-Game-Based Algorithms for Distributed Constraint Optimization Problems.
  * In: Scerri P., Vincent R., Mailler R. (eds)
  * Coordination of Large-Scale Multiagent Systems. Springer, Boston, MA.
  * @param variable which should be valuated
  * @param neighbours the variables which are linked to the variables
  * @param constraints which should be valuated
  */
class AgentBehaviour(variable: Variable,
                     neighbours : Set[Variable],
                     constraints: Set[Constraint],
                     initialContext : Context)
  extends VariableAgent(variable, neighbours, constraints)
    with FSM[MGM2State, MGM2Mind]
    with Stash {

  /**
    * Initially the agent has no task in the bundle
    */
  startWith(Init, new MGM2Mind(initialContext))

  /**
    * Reject some offers
    */
  def rejectOffers(offers: List[Offer]): Unit =
    offers.foreach{ offer: Offer =>
      val neighbour: Variable = offer.offererVariable
      if (trace) println(s"$variable -> $neighbour : Reject($offer)")
      directory.addressOf(neighbour) ! Reject(offer)
    }


  /**
    * Either the agent is in the initial state
    */
  when(Init) {
    //When the agent is triggered by the supervisor
    case Event(Trigger, mind) =>
    if (debug) println(s"$variable in $stateName has received Trigger")
      supervisor = sender
      if (trace) println(s"$variable -> Supervisor : KickStartMe")
      supervisor ! KickStartMe
      unstashAll
      goto(Continue) using mind
        
    // When the agent is informed
    case Event(InformValue(_),mind) =>
      if (debug) println(s"$variable in $stateName has received InformValue and stashed it")
      stash
      stay using mind
  }

  /**
    * Either the agent is in the continue state
    */
  when(Continue) {

    //
    case Event(MakeOffer(_), mind) => {
      if (debug) println(s"$variable in $stateName has received MakeOffer and stashed it")
      stash
      stay using mind
    }

    case Event(Reject(_), mind) => {
      if (debug) println(s"$variable in $stateName has received Reject and ignores it")
      stay using mind
    }

    case Event(InformDelta(_), mind) => {
      if (debug) println(s"$variable in $stateName has received InformDelta and stashed it")
      stash
      stay using mind
    }

    //When the supervisor tells it to stop
    case Event(StopAlgo, mind) =>
      if (debug) println(s"$variable in $stateName has StopAlgo")
      goto(Final) using mind


    //When the supervisor tells it to continue
    case Event(ContinueAlgo, mind) =>
      if (debug) println(s"$variable in $stateName has received ContinueAlgo")
      val updatedMind = mind.reset(variable) // performing a reset
      broadcast(InformValue(updatedMind.context.getValue(variable).get))
      val b: Boolean = mind.determineSubset(AgentBehaviour.THRESHOLD)
      if (trace) println(s"$variable -> $variable : ChooseSubset($b)")
      directory.addressOf(variable) ! ChooseSubset(b)
      unstashAll
      goto(WaitingForRole) using updatedMind

    // When the partner from previous round tells it to Go
    case Event(GiveGo(_), mind) =>
      if (debug) println(s"$variable in $stateName has received GiveGo and ignores it")
      stay using mind

    // When the partner from previous round tells it to not Go
    case Event(GiveNoGo, mind) =>
      if (debug) println(s"$variable in $stateName has received GiveNoGo and ignores it")
      stay using mind

    // When the partner gets an informValue
    case Event(InformValue(_), mind) =>
      if (debug) println(s"$variable in $stateName has received InformValue and stahes it")
      stash
      stay using mind
  }

 /**
  * Either the agent is in the WaitingForRole state
  */
  when(WaitingForRole){

    case Event(InformDelta(_), mind) => {
      if (debug) println(s"$variable in $stateName has received InformDelta and stashed it")
      stash
      stay using mind
    }

    case Event(MakeOffer(_), mind) => {
      if (debug) println(s"$variable in $stateName has received MakeOffer and stashed it")
      stash
      stay using mind
    }

    // When it receives an InformValue
    case Event(InformValue(_),mind) =>
      if (debug) println(s"$variable in $stateName has received InformValue and stashed it")
      stash
      stay using mind

    // When it receives a ChooseSubset(true) and becomes an Offerer
    case Event(ChooseSubset(true), mind) =>
      if (debug) println(s"$variable in $stateName has received ChooseSubset(true)")
      unstashAll
      goto(OffererWaitingValues) using mind
    
    // When it receives a ChooseSubset(false) and becomes a Receiver
    case Event(ChooseSubset(false), mind) =>
      if (debug) println(s"$variable in $stateName has received ChooseSubset(false)")
      broadcast(MakeOffer(new Offer(variable, List())))
      unstashAll
      goto(ReceiverWaitingValues) using mind
  }

/**
  * Either the agent is in the OffererWaitingValues state
  */
  when(OffererWaitingValues){

    case Event(InformDelta(_), mind) => {
      if (debug) println(s"$variable in $stateName has received InformDelta and stashed it")
      stash
      stay using mind
    }

    // When the context is partial, the agent is waiting for some InformValue message
    case Event(InformValue(peerValue), mind) if mind.context.size < neighbours.size  =>
    if (debug) println(s"$variable in $stateName has received InformValue and context is partial.")
      val peerVariable = directory.variableOf(sender)
      val updatedMind: MGM2Mind = mind.updateContext(variable, peerVariable, peerValue)
      if (debug) println(s"$variable: the value of $peerVariable is $peerValue " +
        s"(${updatedMind.context.size-1}/${neighbours.size})")
      stay using updatedMind
    
    // When the context is complete, the agent chooses a partner to send an offer
    case Event(InformValue(peerValue),mind) if mind.context.size == neighbours.size  =>
      if (debug) println(s"$variable in $stateName has received InformValue and context is full")
      val peerVariable = directory.variableOf(sender)
      var updatedMind: MGM2Mind = mind.updateContext(variable, peerVariable, peerValue)
      if (debug) println(s"$variable: the value of $peerVariable is $peerValue " +
        s"(${updatedMind.context.size-1}/${neighbours.size})")
      val potentialPartner: Variable = updatedMind.choosePartner(neighbours)
      updatedMind = updatedMind.setPartner(potentialPartner)
      if (debug) println(s"$variable> context before computing the joint offer=${updatedMind.currentContext}")
      val moves: List[BilateralMove] = updatedMind.computeJointOffer(variable, potentialPartner, constraints)
      val offer: Offer = new Offer(variable, moves)
      directory.addressOf(potentialPartner) ! MakeOffer(offer)
      if (trace) println(s"$variable -> $potentialPartner : MakeOffer(${Some(offer)}")
      multicast(neighbours - potentialPartner, MakeOffer(new Offer(variable, List())))
      unstashAll
      goto(OffererMakingOff) using updatedMind

    // When it receives an offer, the agent rejects it
    case Event(MakeOffer(offer), mind) =>
      if (debug) println(s"$variable in $stateName has received MakeOffer")
      val neighbour: Variable = directory.variableOf(sender)
      if (trace) println(s"$variable -> $neighbour : Reject(${Some(offer)})")
      directory.addressOf(neighbour) ! Reject(offer)
      stay using mind
  }

  /**
  * Either the agent is in the OffererMakingOff state
  */
  when(OffererMakingOff){

    //when it receives a neighbour's delta
    case Event(InformDelta(_), mind) =>
      if (debug) println(s"$variable in $stateName has received InformDelta and stashed it")
      stash
      stay using mind
    
    //when it receives an offer, the agent rejects it
    case Event(MakeOffer(offer), mind) =>
      if (debug) println(s"$variable in $stateName has received MakeOffer")
      val neighbour: Variable = directory.variableOf(sender)
      if (trace) println(s"$variable -> $neighbour : Reject(${Some(offer)})")
      directory.addressOf(neighbour) ! Reject(offer)
      stay using mind
    
    // When it receives an acceptance from its potential partner
    case Event(Accept(offer), mind) =>
      if (debug) println(s"$variable in $stateName has received Accept")
      val newPartner: Variable = directory.variableOf(sender)
      var updatedMind: MGM2Mind = mind.commitFully(newPartner, offer)
      updatedMind = updatedMind.updateDeltas(variable: Variable, offer.moves.head.payoff)
      updatedMind = updatedMind.setCurrBestOffer(Some(offer))
      broadcast(InformDelta(updatedMind.deltas(variable)))
      unstashAll
      goto(Committed) using updatedMind

    // When it receives a refusal from its potential partner
    case Event(Reject(_), mind) if directory.variableOf(sender) == mind.partner.get => //TODO check sender partner
      if (debug) println(s"$variable in $stateName has received Reject")
      var updatedMind = mind.uncommit()
      val (bestMove, bestDelta) = updatedMind.bestUnilateralMove(variable, constraints)
      val bestOffer = bestMove match {
        case Some(m) => Some(new Offer(variable, List(m)))
        case None => None
      }
      updatedMind = updatedMind.updateDeltas(variable: Variable, bestDelta)
      updatedMind = updatedMind.setCurrBestOffer(bestOffer)
      broadcast(InformDelta(updatedMind.deltas(variable)))
      unstashAll
      goto(Uncommitted) using updatedMind

    // TODO When it receives a refusal from a peer which is not a potential partner
    case Event(Reject(_), mind) if directory.variableOf(sender) != mind.partner.get =>
      if (debug) println(s"$variable in $stateName has received Reject from a non-partner so it ignores it")
      stay using mind
  }

 /**
  * Either the agent is in the ReceiverWaitingValues state
  */
  when(ReceiverWaitingValues) {

    case Event(InformDelta(_), mind) => {
      if (debug) println(s"$variable in $stateName has received InformDelta and stashes it")
      stash
      stay using mind
    }

        // When it receives a reject it does NOTHING
    case Event(Reject(_), mind) =>
      if (debug) println(s"$variable in $stateName has received Reject and ignores it")
      stay using mind

    // When the context is partial
    case Event(InformValue(peerValue), mind) if mind.context.size < neighbours.size  =>
      if (debug) println(s"$variable in $stateName has received InformValue and the context is partial.")
      val peerVariable = directory.variableOf(sender)
      val updatedMind: MGM2Mind = mind.updateContext(variable, peerVariable, peerValue)
      if (debug) println(s"$variable: the value of $peerVariable is $peerValue (${updatedMind.context.size-1}/${neighbours.size})")
      stay using updatedMind
    
    // When the context is complete
    case Event(InformValue(peerValue),mind) if mind.context.size == neighbours.size  =>
      if (debug) println(s"$variable in $stateName has received InformValue and the context is full.")
      val peerVariable = directory.variableOf(sender)
      var updatedMind: MGM2Mind = mind.updateContext(variable, peerVariable, peerValue)
      updatedMind = updatedMind.setPartner(updatedMind.choosePartner(neighbours))
      if (debug) println(s"$variable: the value of $peerVariable is $peerValue (${updatedMind.context.size-1}/${neighbours.size})")
      unstashAll
      goto(ReceiverWaitingOffers) using updatedMind

    // When it receives an offer it stashes it
    case Event(MakeOffer(_), mind) =>
      if (debug) println(s"$variable in $stateName has received MakeOffer and stashes it")
      stash
      stay using mind
  }

  /**
  * Either the agent is in the ReceiverWaitingOffers state
  */
  when(ReceiverWaitingOffers) {

    case Event(Reject(_), mind) => {
      if (debug) println(s"$variable in $stateName has received Reject from a non-partner and ignores it")
      stay using mind
    }
    
    // When offers are still missing
     case Event(MakeOffer(eventualOffer), mind) if mind.nbReceivedOffers < neighbours.size-1 =>
     if (debug) println(s"$variable in $stateName has received MakeOffer and the context is partial.")
       val updatedMind: MGM2Mind = mind.addOffer(eventualOffer)
      stay using updatedMind

    // When the last offer is received
     case Event(MakeOffer(eventualOffer), mind) if mind.nbReceivedOffers == neighbours.size-1 =>
     if (debug) println(s"$variable in $stateName has received MakeOffer and the context is full.")
      var updatedMind: MGM2Mind = mind.addOffer(eventualOffer)
      val (bestOffer, bestDelta) = updatedMind.chooseBestSingleOffer(variable, constraints)
       updatedMind = updatedMind.updateDeltas(variable, bestDelta)
       val partner : Option[Variable]= bestOffer match {
         case Some(o) => Some(o.offererVariable)
         case None => None
       }
      updatedMind = updatedMind.setCurrBestOffer(bestOffer)
      if (trace) println(s"$variable -> $variable : BestOffer($bestOffer)")
      self ! BestOffer(bestOffer)
      unstashAll
      goto(ReceiverAllOffersReceived) using updatedMind

      // When it receives a neighbour's delta
    case Event(InformDelta(_), mind) =>
      if (debug) println(s"$variable in $stateName has received InformDelta")
      stash
      stay using mind
  }

  /**
  * Either the agent is in the ReceiverAllOffersReceived state
  */
  when(ReceiverAllOffersReceived) {

    // When it receives a reject it does NOTHING
    case Event(Reject(_), mind) =>
      if (debug) println(s"$variable in $stateName has received Reject and ignores it")
      stay using mind

    // When it receives a neighbour's delta
    case Event(InformDelta(_), mind) =>
      if (debug) println(s"$variable in $stateName has received InformDelta and stashes it")
      stash
      stay using mind

    // When there is a best coordinated move
    case Event(BestOffer(Some(bestOffer)), mind) =>
      if (debug) println(s"$variable in $stateName has received BestOffer")
      if (trace) println(s"$variable -> ${bestOffer.offererVariable} : Accept($bestOffer)")
      directory.addressOf(bestOffer.offererVariable) ! Accept(bestOffer)
      val updatedMind: MGM2Mind = mind.commitFully(bestOffer.offererVariable, bestOffer)
      rejectOffers(updatedMind.receivedOffers.filterNot(_.offererVariable == bestOffer.offererVariable))//DONE decline to the other OFFERERS
      broadcast(InformDelta(updatedMind.deltas(variable))) //inform all neighbours
      unstashAll
      goto(Committed) using updatedMind

    // When there is no coordinated move a unilateral move is performed
    case Event(BestOffer(None), mind) =>
    if (debug) println(s"$variable in $stateName has received BestOffer(empty)")
      val updatedMind: MGM2Mind = mind.uncommit()
      rejectOffers(updatedMind.receivedOffers) //decline all offers
      broadcast(InformDelta(updatedMind.deltas(variable))) //inform all neighbours
      unstashAll
      goto(Uncommitted) using updatedMind
  }

  /**
  * Either the agent is in the Committed state
  */
  when(Committed) {

    //when it receives its partner's giveGo
    case Event(GiveGo(_), mind) => {
      if (debug) println(s"$variable in $stateName has received GiveGo and stashes it")
      stash
      stay using mind
    }

    //when it receives its partner's giveGo
    case Event(GiveNoGo, mind) => {
      if (debug) println(s"$variable in $stateName has received GiveNoGo and stashes it")
      stash
      stay using mind
    }

        // When it receives a reject it does NOTHING
    case Event(Reject(_), mind) =>
      if (debug) println(s"$variable in $stateName has received Reject and ignores it")
      stay using mind

     // When it receives an offer it does NOTHING
    case Event(MakeOffer(someOffer), mind) =>
      if (debug) println(s"$variable in $stateName has received MakeOffer")
        sender ! Reject(someOffer)
      stay using mind

    //when it receives a neighbour's delta and the neighbourDeltas is partial
    case Event(InformDelta(delta), mind) if mind.deltas.size < neighbours.size  =>
    if (debug) println(s"$variable in $stateName has received InformDelta (${mind.deltas.size}/${neighbours.size}) and the contextis partial.")
      val neighbour: Variable = directory.variableOf(sender)
      val updatedMind: MGM2Mind = mind.updateDeltas(neighbour: Variable, delta: Double)
      stay using updatedMind

    //when it receives a neighbour's delta and the context is full
    case Event(InformDelta(delta), mind) if mind.deltas.size == neighbours.size =>
    if (debug) println(s"$variable in $stateName has received InformDelta (${mind.deltas.size}/${neighbours.size}) and the context is full.")
      val neighbour: Variable = directory.variableOf(sender)
      val updatedMind: MGM2Mind = mind.updateDeltas(neighbour: Variable, delta: Double)
      val shouldIact: Boolean = updatedMind.amIBest(variable)
      val bestOffer: Option[Offer] = updatedMind.currBestOffer match {
       case Some(o) => Some(o)
       case None => throw new RuntimeException(s"Error $variable should have a best offer in $stateName")
      }
      val bestMove : Move = bestOffer.get.moves.head
      if (trace) println(s"$variable -> $variable : Act($shouldIact)")
      self ! Act(shouldIact, Some(bestMove))
      unstashAll
      goto(GivingPartnerGoNogo) using updatedMind
  }

  /**
  * Either the agent is in the Uncommitted state
  */
  when(Uncommitted){
    // When it receives a reject it does NOTHING
    case Event(Reject(_), mind) =>
      if (debug) println(s"$variable in $stateName has received Reject and ignores it")
      stay using mind

    // When it receives an offer it does NOTHING
    case Event(MakeOffer(someOffer), mind) =>
      if (debug) println(s"$variable in $stateName has received MakeOffer and ignores it")
      sender ! Reject(someOffer)
      stay using mind

    // When it receives a neighbour's delta and the neighbourDeltas is partial
    case Event(InformDelta(delta), mind) if mind.deltas.size < neighbours.size  =>
    if (debug) println(s"$variable in $stateName has received InformDelta (${mind.deltas.size}/${neighbours.size}) and the context is partial.")
      val neighbour: Variable = directory.variableOf(sender)
      val updatedMind: MGM2Mind = mind.updateDeltas(neighbour: Variable, delta: Double)
      stay using updatedMind

    // When it receives a neighbour's delta and the context is full
    case Event(InformDelta(delta), mind) if mind.deltas.size == neighbours.size =>
    if (debug) println(s"$variable in $stateName has received InformDelta  (${mind.deltas.size}/${neighbours.size})and the context is full.")
      val neighbour: Variable = directory.variableOf(sender)
      val updatedMind: MGM2Mind = mind.updateDeltas(neighbour: Variable, delta: Double)
      val shouldIact: Boolean = updatedMind.amIBest(variable)
      val bestUnilateralMove = updatedMind.bestUnilateralMove(variable, constraints)._1
      if (trace) println(s"$variable -> $variable : Act($shouldIact)")
      self ! Act(shouldIact, bestUnilateralMove)
      unstashAll
      goto(ActSolo) using updatedMind
  }

  /**
  * Either the agent is in the ActSolo state
  */
  when(ActSolo){

      // When it receives a reject it does NOTHING
    case Event(Reject(_), mind) =>
      if (debug) println(s"$variable in $stateName has received Reject and ignores it")
      stay using mind

    // When it should act because it has the highest delta in the neighbourhood
    case Event(Act(true, Some(move)), mind) =>
    if (debug) println(s"$variable in $stateName has received Act(true)")
    val newVal: Value = mind.updateVal(variable, move)
    val updatedMind: MGM2Mind = mind.updateContext(variable, variable, newVal)
    if (trace) println(s"$variable -> Supervisor : I changed = InformValue(${updatedMind.context.getValue(variable).get})")
    supervisor ! InformValue(updatedMind.context.getValue(variable).get)
    goto(Continue) using updatedMind

    // When it should act because it has the highest delta in the neighbourhood
    case Event(Act(true, None), mind) =>
    if (debug) println(s"$variable in $stateName has received Act(true) but has no move")
    if (trace) println(s"$variable -> Supervisor : I kept my previous value = InformValue(${mind.context.getValue(variable).get})")
    supervisor ! InformValue(mind.context.getValue(variable).get)
    unstashAll
    goto(Continue) using mind
    
    // When it should not act because it does not have the highest delta in the neighbourhood
    case Event(Act(false, _), mind) =>
    if (debug) println(s"$variable in $stateName has received Act(false)")
    if (trace) println(s"$variable -> Supervisor : I didn't change = InformValue(${mind.context.getValue(variable).get})")
    supervisor ! InformValue(mind.context.getValue(variable).get)
    unstashAll
    goto(Continue) using mind
  }

  /**
  * Either the agent is in the GivingPartnerGoNogo state
  */
  when(GivingPartnerGoNogo){

    // When it receives its own acting decision and should not act
    case Event(Act(false, _), mind) =>
      if (debug) println(s"$variable in $stateName has received Act(false)")
    if (trace) println(s"$variable -> Supervisor : I didn't change = InformValue(${mind.context.getValue(variable).get})")
      supervisor ! InformValue(mind.context.getValue(variable).get)
      val partner: Variable = mind.partner match {
         case Some(p) => p
         case None => throw new RuntimeException(s"Error $variable is committed but lacks a partner in state $stateName")
       }
      directory.addressOf(partner) ! GiveNoGo
      unstashAll
      goto(Continue) using mind

    // When it receives its own acting decision and should act
    case Event(Act(true, Some(move)), mind) =>
      if (debug) println(s"$variable in $stateName has received Act(true)")
      unstashAll
      val partner: Variable = mind.partner match {
         case Some(p) => p
         case None => throw new RuntimeException(s"Error $variable is committed but lacks a partner in state $stateName")
       }
      if (trace) println(s"$variable -> ${partner} : GiveGo($move)")
      directory.addressOf(partner) ! GiveGo(move)
      unstashAll
      goto(HandlingPartnersGoNogo) using mind

    // When its partner tells it to go
    case Event(GiveGo(_), mind) =>
      if (debug) println(s"$variable in $stateName has received GiveGo and stashes it")
      stash
      stay using mind

    // When its partner tells it not to go
    case Event(GiveNoGo, mind) =>
      if (debug) println(s"$variable in $stateName has received GiveNoGo and stashes it")
      stash
      stay using mind
  }

  /**
  * Either the agent is in the GivingPartnerGoNogo state
  */
  when(HandlingPartnersGoNogo){

    // When it receives a Go from its partner
    case Event(GiveGo(move), mind) =>
      if (debug) println(s"$variable in $stateName has received GiveGo")
      val newVal: Value = mind.updateVal(variable, move)
      val updatedMind: MGM2Mind = new MGM2Mind(context = mind.context.fix(variable, newVal),
        mind.isCommitted, mind.partner,
        mind.receivedOffers, mind.nbReceivedOffers,
        mind.deltas,
        mind.metric)
      if (trace) println(s"$variable -> Supervisor : I'm changing = InformValue(${updatedMind.context.getValue(variable).get})")
      supervisor ! InformValue(updatedMind.context.getValue(variable).get)
      unstashAll
      goto(Continue) using updatedMind

    // When it receives a NoGo from its partner
    case Event(GiveNoGo, mind) =>
      if (debug) println(s"$variable in $stateName has received GiveNoGo")
      if (trace) println(s"$variable -> Supervisor : I'm not changing = InformValue(${mind.context.getValue(variable).get})")
      supervisor ! InformValue(mind.context.getValue(variable).get)
      unstashAll
      goto(Continue) using mind
  }


  /**
    * Whatever the state is
    **/
  whenUnhandled {

    // If the agent is killed
    case Event(Query, mind) =>
      sender ! Inform(mind.metric)
      context.stop(self)
      stay using mind

    // If the agent is enlightened with the acquaintances it is no more blind
    case Event(Light(directory), mind)  =>
      supervisor = sender
      this.directory = directory
      sender ! Ready
      stay using mind

    // If the agent must trace the protocol
    case Event(Trace, mind)  =>
      if (debug) println("Agent receives Trace")
      this.trace = true
      stay using mind

    // Other
    case Event(msg @ _, _) =>
      throw new RuntimeException(s"Error $variable was not expected message $msg in state $stateName")
  }


  /**
    * Associates actions with a transition instead of with a state and even, e.g. debugging
    */
  onTransition {
    case s1 -> s2 =>
      if (debugState) println(s"$variable moves from the state $s1 to the state $s2")
  }

  // Finally Triggering it up using initialize, which performs
  // the transition into the initial state and sets up timers (if required).
  initialize()
}

/**
  * Companion object for [AgentBehaviour]
  */
object AgentBehaviour{
  val THRESHOLD = 0.5
}
