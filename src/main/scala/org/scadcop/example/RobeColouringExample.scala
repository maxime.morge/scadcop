// Copyright (C) Maxime MORGE & Alex VIGNERON 2020
package org.scadcop.example

import org.scadcop.problem._

/**
  * Object representing the robe colouring system
  */
object RobeColouringExample {
  val white : NominalValue = NominalValue("white")
  val black : NominalValue = NominalValue("black")
  val colours = List(black, white)
  val gandalf = new Variable(name = "gandalf", colours)
  val aragorn = new Variable(name = "aragorn", colours)
  val frodo = new Variable(name = "frodo", colours)

  val costGandalfAragorn : Array[Array[Double]] =
    Array(Array(3.0, 0.0), Array(8.0, 5.0))
  val costGandalfFrodo : Array[Array[Double]] =
    Array(Array(1.0, 0.0), Array(5.0, 6.0))
  val costFrodoGandalf : Array[Array[Double]] =
    Array(Array(4.0, 0.0), Array(3.0, 1.0))

  val constraintGandalfAragorn = new Constraint(gandalf, aragorn, costGandalfAragorn)
  val constraintGandalfFrodo = new Constraint(gandalf, frodo, costGandalfFrodo)
  val constraintFrodoGandalf = new Constraint(frodo, aragorn, costFrodoGandalf)
  val pb = new DCOP(Set(gandalf, aragorn, frodo), Set(constraintGandalfAragorn, constraintGandalfFrodo, constraintFrodoGandalf))

  val initialContext : Context= new Context()
    .fix(Map(gandalf -> black, aragorn -> black, frodo -> black))
}
