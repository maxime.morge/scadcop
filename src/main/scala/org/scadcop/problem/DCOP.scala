// Copyright (C) Maxime MORGE 2020
package org.scadcop.problem

/**
  * Class representing a Distributed Constraint Optimization Problem
  * @param variables for which values must be given
  * @param constraints represented as cost functions
  */
class DCOP(val variables: Set[Variable], val constraints: Set[Constraint]) {

  val debug = false

  /**
    * String representation
    */
  override def toString: String = s"Variables:\n"+
    variables.map(v => v.description).mkString("\t", "\n\t", "\n")+
  "Constraints:\n"+
    constraints.mkString("\t", "\n\t", "\n")

  /**
    * Returns the size of the DCOP
    */
  def size = variables.size

  /**
    * Returns true if the DCOP is sound, i.e.
    * 1 -there is at least one variable,
    * 2- each variable has a unique id
    * all the constraints are sound
    */
  def isSound : Boolean = variables.nonEmpty &&
    variables.map(_.name).size == variables.size &&
    constraints.forall( c => c.sound())

  /**
    * Returns the other variables linked to the variable
    */
  def linkedVariablesOf(variable : Variable) :  Set[Variable] = {
    var variables =  Set[Variable]()
    constraints.foreach{ constraint =>
      if (constraint.varA == variable)  variables = variables + constraint.varB
      if (constraint.varB == variable)  variables = variables + constraint.varA
    }
    if (debug) println(s"DCOP: linked variables of $variable=$variables")
    variables
  }

  /**
    * Returns the constraints over a specific variable
    */
  def constraintsOf(variable: Variable) : Set[Constraint] =
    constraints.filter(c => c.varA == variable || c.varB == variable)

  /**
    * Returns true if a context is sound,
    * i.e. each value is in the corresponding domain
    */
  def isSound(context: Context) : Boolean = {
    variables.foreach{ variable =>
      if (! variable.domain.contains(context.valuation(variable))) return  false
    }
    true
  }

  /**
    * Returns true if the context is an assignment, i.e. a full context
    */
  def isFull(context: Context): Boolean = context.valuation.size == variables.size

  /**
    * Returns the objective value of an assignment, i.e. a full context
    */
  def objective(context: Context) : Double = {
    if (! isFull(context))
      throw new RuntimeException(s"Error: the context $context is not a (full) assignment")
    constraints.foldLeft(0.0)((acc, constraint) =>
      acc + constraint.cost(constraint.varA, context.valuation(constraint.varA), constraint.varB, context.valuation(constraint.varB)))
  }
}

