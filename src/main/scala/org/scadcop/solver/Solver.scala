// Copyright (C) Maxime MORGE 2020
package org.scadcop.solver

import org.scadcop.problem._
import org.scadcop.util.MyTime

/**
  * Abstract class representing a solver
  * @param pb DCOP instance
  */
abstract class Solver(val pb : DCOP) {
  var debug = false
  var trace = false

  def name() : String

  var solvingTime : Long = 0

  override def toString: String = s"${name()} (${MyTime.show(solvingTime)})"


  /**
    * Returns an assignment
    */
  protected def solve() : Context

  /**
    * Returns an assignment and update solving time
    */
  def run() : Context = {
    val startingTime = System.nanoTime()
    val assignment = solve()
    solvingTime = System.nanoTime() - startingTime
    if (! pb.isSound(assignment))
      throw new RuntimeException(s"Solver: the outcome\n $assignment\n for\n $pb is not sound")
    if (! pb.isFull(assignment))
      throw new RuntimeException(s"Solver: the outcome\n $assignment\n for\n $pb is not full")
    assignment
  }
}
