// Copyright (C) Maxime MORGE, Alex VIGNERON 2020
package org.scadcop.solver.decentralized.agent

import org.scadcop.problem._

/**
  *  Class representing the state of mind for a [[VariableAgent]]
  *  @param currentContext current assignment of the variable and the linked ones
  */
class Mind(val currentContext : Context = new Context())
